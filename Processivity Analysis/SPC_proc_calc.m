function [proc_seg, proc_av, force_seg, force_av] = SPC_proc_calc(indx, DNAext, force)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

a = size(indx,1);

proc_seg.all = zeros(a,1);   % initialize variables

force_seg.all = zeros(a,1);

for i = 1:1:a
                                              
    proc_seg.all(i,1) = DNAext(indx(i,2)) - DNAext(indx(i,1));
    
    force_temp = force(indx(i,1):indx(i,2));  % get processivity by difference in extension
    
    force_seg.all(i,1) = mean(force_temp);    % also get force average over processivity interval
    
end

proc_seg.all_round = round(proc_seg.all);  % round processivity values to nearest whole number

force_trace_av = mean(force); % get force average for whole trace

proc_seg.all_pos = proc_seg.all(proc_seg.all > 0); % separate unrounded processivity into positive and negative

proc_seg.all_neg = abs(proc_seg.all(proc_seg.all < 0));

proc_seg.all_round_pos = proc_seg.all_round(proc_seg.all_round > 0); % separate rounded processivity into positive and negative

proc_seg.all_round_neg = abs(proc_seg.all_round(proc_seg.all_round < 0));

force_seg.all_pos = force_seg.all(proc_seg.all > 0); % separate forces corresponding to positive and negative regions

force_seg.all_neg = force_seg.all(proc_seg.all < 0);

force_seg.all_trace_mean = force_trace_av*ones(a,1); % get vector of just the mean force for whole trace

force_seg.all_trace_mean_pos = force_seg.all_trace_mean(proc_seg.all > 0); % separate vector of repeats of mean force for whole trace into positive and negative

force_seg.all_trace_mean_neg = force_seg.all_trace_mean(proc_seg.all < 0);

proc_av.all_pos_av = mean(proc_seg.all_pos); % get averages for unrounded processivities

proc_av.all_neg_av = mean(proc_seg.all_neg);

proc_av.round_pos_av = mean(proc_seg.all_round_pos); % get averages for rounded processivities

proc_av.round_neg_av = mean(proc_seg.all_round_neg);

force_av.pos = mean(force_seg.all_pos); % get averages of forces over different processivity intervals

force_av.neg = mean(force_seg.all_neg);

end

