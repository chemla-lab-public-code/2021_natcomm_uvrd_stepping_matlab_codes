function [proc_seg_tot, proc_av_tot, force_seg_tot, force_av_tot] = SPC_proc_tot_compile
% Short function to compile processivity data after it has all
% been analyzed.  To run, make sure the .dat files for analyzed
% processivity data are all in one folder, specify the name of that folder 
% below, and run the program.

fpath = 'C:\Users\seanc\Documents\MutL plus UvrD hairpin data most important stuff\10 uM ATP\UvrD plus MutL\data files_corr\';
MATLABCodePath = 'C:\Users\seanc\Documents\Matlab codes\';
d = dir([fpath '*.mat']);

proc_seg_tot.pos = [];
proc_seg_tot.neg = [];
proc_seg_tot.pos_round = [];
proc_seg_tot.neg_round = [];
proc_av_tot.pos = [];
proc_av_tot.neg = [];
proc_av_tot.pos_round = [];
proc_av_tot.neg_round = [];

force_seg_tot.pos = [];
force_seg_tot.neg = [];
force_seg_tot.pos_trace_av = [];
force_seg_tot.neg_trace_av = [];
force_av_tot.pos = [];
force_av_tot.neg = [];

for k = 1:1:length(d)
    cd(fpath)
    name = [d(k).name];
    load(name);

    proc_seg_tot.pos = [proc_seg_tot.pos, proc_seg.all_pos'];
    proc_seg_tot.neg = [proc_seg_tot.neg, proc_seg.all_neg'];
    proc_seg_tot.pos_round = [proc_seg_tot.pos_round, proc_seg.all_round_pos'];
    proc_seg_tot.neg_round = [proc_seg_tot.neg_round, proc_seg.all_round_neg'];
    proc_av_tot.pos = [proc_av_tot.pos, proc_av.all_pos_av'];
    proc_av_tot.neg = [proc_av_tot.neg, proc_av.all_neg_av'];
    proc_av_tot.pos_round = [proc_av_tot.pos_round, proc_av.round_pos_av'];
    proc_av_tot.neg_round = [proc_av_tot.neg_round, proc_av.round_neg_av'];
    
    force_seg_tot.pos = [force_seg_tot.pos, force_seg.all_pos'];
    force_seg_tot.neg = [force_seg_tot.neg, force_seg.all_neg'];
    force_seg_tot.pos_trace_av = [force_seg_tot.pos_trace_av, force_seg.all_trace_mean_pos'];
    force_seg_tot.neg_trace_av = [force_seg_tot.neg_trace_av, force_seg.all_trace_mean_neg'];
    force_av_tot.pos = [force_av_tot.pos, force_av.pos'];
    force_av_tot.neg = [force_av_tot.neg, force_av.neg'];
    
end

cd(MATLABCodePath);

end

