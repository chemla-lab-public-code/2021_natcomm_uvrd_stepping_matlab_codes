function [proc_ind] = SPC_get_proc_ind(proc_ind_start)
% Function to automate process of selecting segments for getting
% processivity

fig = gcf;
selectedData = datacursormode(fig);
pause; datacursormode off
vals = getCursorInfo(selectedData);
indarray(1) = vals(1).DataIndex;
indarray(2) = vals(2).DataIndex;
indarray = sort(indarray);

proc_ind = proc_ind_start;
proc_ind = vertcat(proc_ind,indarray);


end

