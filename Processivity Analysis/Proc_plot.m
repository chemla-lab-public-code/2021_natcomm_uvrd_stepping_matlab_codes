function [trendline] = Proc_plot(indx,time,DNAext)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

a = size(indx,1);

trendline.xvals = zeros(a,2);

trendline.yvals = zeros(a,2);

for i = 1:1:a
    
%     pts = zeros(1,2);
%     
%     xdelt = time(indx(i,2)) - time(indx(i,1));
%     
%     ydelt = DNAext(indx(i,2)) - DNAext(indx(i,1));
%celar      
%     slope = ydelt/xdelt;
%     
%     int = (-1*slope*time(i,1)) + DNAext(i,1);
%     
%     pts(1,1) = DNAext(indx(i,1));
%     
%     pts(1,2) = DNAext(indx(i,2));
%     

time_vec = [time(indx(i,1)), time(indx(i,2))];

ext_vec = [DNAext(indx(i,1)), DNAext(indx(i,2))];

trendline.xvals(i,1) = time_vec(1);

trendline.xvals(i,2) = time_vec(2);

trendline.yvals(i,1) = ext_vec(1);

trendline.yvals(i,2) = ext_vec(2);
    
plot(time_vec,ext_vec,'r--')

plot(time_vec,ext_vec,'ro')

end





end

