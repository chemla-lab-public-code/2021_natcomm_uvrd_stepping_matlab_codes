function [dist_tot, dist_pos, dist_neg] = p_dist(data)
% p_dist, short function to compute pairwise distance for a vector of
% data and separate the pairwise distance data into positive and negative
% values

a = length(data);

data = data';

Dist = [];

for i = 1:1:(a-1)
    
   c = data(i);
    
   d = data(i+1:end) - c;
   
   Dist = [Dist; d];
   
end

dist_tot = abs(Dist);

dist_pos = Dist(Dist > 0);

dist_neg = Dist(Dist < 0);

end





