function [pdist_all, pdist_pos, pdist_neg] = pdist_compile
% Short function to go through the data for all intervals of fitted steps,
% calculate the pairwise distance data (considering both positive and
% negative pairwise distances) for each interval, then compile them into
% single vecctors. To use, simply deposit all .dat files corresponding to
% the desired step fit intervals into a single folder and specify that
% fodler as fpath below, then run the script

fpath = 'C:\Users\seanc\Documents\1 uM data all together for pdist\';
MATLABCodePath = 'E:\Matlab codes\';
d = dir([fpath '*.mat']);

pdist_all = [];
pdist_pos = [];
pdist_neg = [];


for k = 1:1:length(d)
    
    cd(fpath)
    name = [d(k).name];
    load(name);                              
    cd(MATLABCodePath)                             % alternate route that first downsamples the data before calculating pwd
    
%     steps_nbp_rs = downsample_data(ydata_rs,4);  
    
%     [tot_dist_tr, pos_dist_tr, neg_dist_tr] = p_dist(steps_nbp_rs');

     [tot_dist_tr, pos_dist_tr, neg_dist_tr] = p_dist(steps_nbp);
    
    pdist_all = [pdist_all; tot_dist_tr];
    
    pdist_pos = [pdist_pos; pos_dist_tr];
    
    pdist_neg = [pdist_neg; neg_dist_tr];
    
end

end

