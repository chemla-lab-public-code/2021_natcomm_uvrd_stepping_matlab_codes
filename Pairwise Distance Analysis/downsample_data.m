function out = downsample_data(mat,factor)
%take a vector and downsample it
%090113 mjc

N = length(mat);
Ndesired = floor(N/factor)*factor;

if Ndesired < N
    mat = mat(1:Ndesired); 
end

temp = reshape(mat,factor,floor(N/factor));

if factor > 1
    out = mean(temp);
else
    out = temp;
end