function [tot, pos, neg] = pdist_plotting(pdist_tot, pdist_pos, pdist_neg)
% short function to make plots for total, positive, and negative pairwise
% distribution, given the compiled data.  May occasionally need to adjust
% bin widths for plotting

tot.binwid = 0.4;
[tot.a, tot.b, tot.c] = histcounts(pdist_tot,0:tot.binwid:30);
tot.z = (tot.b(2:end)+tot.b(1:end-1))/2;
tot.sum = sum(tot.a);
tot.prob = tot.a/tot.sum;
tot.pdf = tot.prob/tot.binwid;
figure
% plot(tot.z, tot.prob);
 plot(tot.z, tot.pdf);
xlabel('Distance (bp)')
% ylabel('Probability')
ylabel('Probability Density')
title('5 uM ATP dimer corr pdist tot');

datadirectory = 'C:\Users\seanc\Documents\labelled UvrD stepping\dimer step fits\5 uM data\overall stats\';

fpath_save = datadirectory;
name_save = '5 uM ATP dimer corr pdist tot';
saveas(gca, fullfile(fpath_save, name_save));

pos.binwid = 0.4;
[pos.a, pos.b, pos.c] = histcounts(pdist_pos,0:pos.binwid:30);
pos.z = (pos.b(2:end)+pos.b(1:end-1))/2;
pos.sum = sum(pos.a);
pos.prob = pos.a/pos.sum;
pos.pdf = pos.prob/pos.binwid;
figure
%    plot(pos.z, pos.prob);
    plot(pos.z, pos.pdf);
xlabel('Distance (bp)')
% ylabel('Probability')
   ylabel('Probability Density');
title('5 uM ATP dimer corr pdist pos');

fpath_save = datadirectory;
name_save = '5 uM ATP dimer corr pdist pos';
saveas(gca, fullfile(fpath_save, name_save));
 
neg.binwid = 0.4;
% pdist_neg_abs = abs(pdist_neg);
% [neg.a, neg.b, neg.c] = histcounts(pdist_neg_abs,0:binwid:30);
 [neg.a, neg.b, neg.c] = histcounts(pdist_neg,-30:neg.binwid:0);
neg.z = (neg.b(2:end)+neg.b(1:end-1))/2;
neg.sum = sum(neg.a);
neg.prob = neg.a/neg.sum;
neg.pdf = neg.prob/neg.binwid;
figure
% plot(neg.z, neg.prob)
 plot(neg.z, neg.pdf)
xlabel('Distance (bp)')
%ylabel('Probability')
ylabel('Probability Density')
title('5 uM ATP dimer corr pdist neg');
fpath_save = datadirectory;
name_save = '5 uM ATP dimer corr pdist neg';
saveas(gca, fullfile(fpath_save, name_save));


end

