function [veloc_seg, veloc_av, force_seg, force_av] = velocity_analyze(indx, time, DNAext, force)
% Function to calculate unwinding (and re-zipping) velocities from helicase
% time traces by fitting selected activity segments to a a straight line
% given the time trace data, the indices output from velocity_index_assign, 
% and the force values.

a = size(indx,1);

veloc_seg.all = zeros(a,1); % initialize

int_seg.all = zeros(a,1);

for i = 1:1:a
                                               % goes through and
    time_seg = time(indx(i,1):indx(i,2));      % calculates velocity for
                                               % each index
    DNAext_seg = DNAext(indx(i,1):indx(i,2));
    
    b = polyfit(time_seg,DNAext_seg,1);
    
    vel = b(1);
    
    int_cept = b(2);
    
    veloc_seg.all(i,1) = vel;
    
    int_seg.all(i,1) = int_cept;
    
end

force_seg.all = force*ones(a,1);

veloc_seg.pos = veloc_seg.all(veloc_seg.all > 0); % separate positive and
                                                  % negative velocities
veloc_seg.int_pos = int_seg.all(veloc_seg.all > 0);

veloc_seg.neg = veloc_seg.all(veloc_seg.all < 0);

veloc_seg.int_neg = int_seg.all(veloc_seg.all < 0);

force_seg.pos = force*ones(length(veloc_seg.pos));

force_seg.neg = force*ones(length(veloc_seg.neg));

veloc_av.pos = mean(veloc_seg.pos);

veloc_av.neg = mean(veloc_seg.neg);

force_av.pos = force;

force_av.neg = force;

% hold on            % optional part to plot lines for fitted velocites
                     % on helicase activity time trace
% for j = 1:1:a
% 
%     x = time(indx(j,1):indx(j,2));
% 
%     y = (veloc_seg.all(j)*x) + int_seg.all(j);
% 
%     plot(x,y,'r--','linewidth',1)
%     
%     hold on
% 
% end
% 
% 
end

