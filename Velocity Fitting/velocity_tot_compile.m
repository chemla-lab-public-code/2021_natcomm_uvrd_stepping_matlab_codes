function [spd_seg_tot, spd_av_tot, force_seg_tot, force_av_tot] = velocity_tot_compile
% SPC 8/4/2020 Short function to compile velocity data after it has all
% been analyzed.  To run, make sure the .dat files for analyzed velocity
% data are all in one folder, specify the name of that folder below, and
% run the program.

fpath = 'C:\Users\seanc\Documents\high ATP data\1 mM ATP other analysis method v2\';
MATLABCodePath = 'E:\Matlab codes\';
d = dir([fpath '*.mat']);


spd_seg_tot.pos = [];
spd_seg_tot.neg = [];
spd_av_tot.pos = [];
spd_av_tot.neg = [];
force_seg_tot.pos = [];
force_seg_tot.neg = [];
force_av_tot.pos = [];
force_av_tot.neg = [];


for k = 1:1:length(d)
    cd(fpath)
    name = [d(k).name];
    load(name);
    
    spd_seg_tot.pos = vertcat(spd_seg_tot.pos, veloc_seg.pos);
    spd_seg_tot.neg = vertcat(spd_seg_tot.neg, veloc_seg.neg);
    spd_av_tot.pos = vertcat(spd_av_tot.pos, veloc_av.pos);
    spd_av_tot.neg = vertcat(spd_av_tot.neg, veloc_av.neg);
    force_seg_tot.pos = vertcat(force_seg_tot.pos, force_seg.pos(:,1));
    force_seg_tot.neg = vertcat(force_seg_tot.neg, force_seg.neg(:,1));
    force_av_tot.pos = vertcat(force_av_tot.pos, force_av.pos(:,1));
    force_av_tot.neg = vertcat(force_av_tot.neg, force_av.neg(:,1));

end

end

