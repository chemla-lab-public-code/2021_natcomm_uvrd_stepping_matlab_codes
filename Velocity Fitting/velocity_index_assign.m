function [vel_ind] = velocity_index_assign(vel_ind_start)
% Function to automate the process of selecting segments for velocity fitting
% using the data cursor while interacting with the figure.  To start,
% define vel_ind_start as [].  Then, run this code to get first set of
% indices.  Then keep running this code using vel_ind as the input to
% continually add to the matrix of velocity indices until you are finished
% for a particular trace.

fig = gcf;
selectedData = datacursormode(fig);
pause; datacursormode off
vals = getCursorInfo(selectedData);
indarray(1) = vals(1).DataIndex;
indarray(2) = vals(2).DataIndex;
indarray = sort(indarray);

vel_ind = vel_ind_start;
vel_ind = vertcat(vel_ind,indarray);


end

