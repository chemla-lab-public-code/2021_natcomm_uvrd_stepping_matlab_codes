function [idx,mean_apd,sum_apd,num_photon]=PhotonNum(trapData)

%% Suoang Lu 202015 identify apd SSB fluorescence intensity 
%plot_SSB140(trapData);
plot_OligoExt(trapData);
timeresolution = 0.5;
smoothtrap = round(timeresolution/trapData.sampperiod);
smoothapd = round(timeresolution/trapData.flintperiod);

title('Select Cropmarks')
pause;

timecropmarks = axis;
prompt = 'Num of SSB binding events:';
maxnumchanges = input(prompt);
trapData.cropmarks = axis;

% figure()
% f1 = subplot(2,1,1);
% 
% [WLC_param] = WLC_parameters;
% %   fake_offset = 280; % unit nm;
%     if trapData.scandir == 1
%         bp = 1*WLC_param.hds*XWLCContour(trapData.force_X,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
%         nt = 1*WLC_param.hss*XWLCContour(trapData.force_X,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
%         trapData.OligoExt = trapData.DNAext - 3263*bp - 140*nt; %3263?
%     elseif trapData.scandir == 0 
% %         bp = 1*WLC_param.hds*XWLCContour(trapData.force_Y,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
% %         trapData.DNAext_Y = trapData.DNAext_Y - fake_offset;
% %         trapData.DNAbp = trapData.DNAext_Y./bp;
%     end
%     
% traptimecropmarks = find(trapData.time>timecropmarks(1)&trapData.time<timecropmarks(2));
% plot(trapData.time(traptimecropmarks), smooth(trapData.OligoExt(traptimecropmarks),smoothtrap))
% xlabel('Time(s)');
% ylabel('OligoExt (nm)')
% title([trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' OligoExt(nm) vs Time(s)']);

% f2 = subplot(2,1,2);

apdtimecropmarks = find(trapData.apdtime>timecropmarks(1)&trapData.apdtime<timecropmarks(2));
idx=findchangepts(trapData.apd1(apdtimecropmarks),'Statistic','mean','MaxNumChanges',maxnumchanges*2);

idx2 = [1 idx length(apdtimecropmarks)];
for i = 1:length(idx2)-1
    mean_apd(i) = mean(trapData.apd1(apdtimecropmarks(1)+idx2(i)-1:apdtimecropmarks(1)+idx2(i+1)-1));
    sum_apd(i) = sum(trapData.apd1(apdtimecropmarks(1)+idx2(i)-1:apdtimecropmarks(1)+idx2(i+1)-1));
end
mean_apd = mean_apd*1/1000/trapData.flintperiod;
sum_apd = sum_apd*1/1000/trapData.flintperiod;
% plot(trapData.apdtime(apdtimecropmarks), smooth(trapData.apd1(apdtimecropmarks),smoothapd)*1/1000/trapData.flintperiod)
for i = 1:length(idx2)-1
line([trapData.apdtime(apdtimecropmarks(1)-1+idx2(i)) trapData.apdtime(apdtimecropmarks(1)-1+idx2(i+1))], [mean_apd(i) mean_apd(i)],'Color',[0.85,0.325,0.098])
end
% xlabel('Time(s)');
% ylabel('APD (kHz)')
% title([trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' APD (kHz) vs Time(s)']);

% linkaxes([f1,f2],'x')
