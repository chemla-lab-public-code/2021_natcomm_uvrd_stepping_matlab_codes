# 2021_NatComm_UvrD_Stepping_MATLAB_Codes

Compilation of all essential MATLAB codes necessary for processing and analysis of data measuring the base pair level stepping behavior of E. coli UvrD helicase.  The codes are categorized into several different folders.
