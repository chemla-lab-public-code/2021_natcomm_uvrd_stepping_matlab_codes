function [step_tot, dwell_tot, ff_tot, bb_tot, fb_tot, bf_tot, Dist_total] = step_data_compile
% Function that compiles all step dwell, and pairwise distance data for one
% for one ATP concentration into single matrices.  The .dat files with the
% matrices for individual intervals fo fitted steps need to be placed in a
% single folder, specified below, and then the script can be run to compile
% the data together.  

fpath = 'E:\2019 p5 uM data\plots v3\all data together v2\';
MATLABCodePath = 'C:\Users\chemlalab\Documents\MATLAB\';  % define analysis path and path for folder with the data in question
d = dir([fpath '*.mat']);

Dist_total.all_pdist = []; % for pdist, do pdist all together, and also positive and negative separately
Dist_total.pos_pdist = [];  
Dist_total.neg_pdist = [];
step_tot = [];  
dwell_tot = [];
ff_tot = [];  % compile data for all 4 dwell types
bb_tot = [];
fb_tot = [];
bf_tot = [];


for k = 1:1:length(d)
    
    cd(fpath)
    name = [d(k).name];
    load(name);
    cd(MATLABCodePath)
    
    Dist_total.all_pdist = [Dist_total.all_pdist, pdist.dist_tot'];
    Dist_total.pos_pdist = [Dist_total.pos_pdist, pdist.dist_pos'];
    Dist_total.neg_pdist = [Dist_total.neg_pdist, pdist.dist_neg'];
    step_tot = [step_tot; step_mat];
    dwell_tot = [dwell_tot; dwell_mat];
    ff_tot = [ff_tot; ff_dwell];
    bb_tot = [bb_tot; bb_dwell];
    fb_tot = [fb_tot; fb_dwell];
    bf_tot = [bf_tot; bf_dwell];
    

end
end

