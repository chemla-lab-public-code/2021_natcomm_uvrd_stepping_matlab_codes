 % to use this code, first enter all the info for the relevant data file
 % at the top and make sure that you have loaded the data for the specific
 % time trace being analyzed as your variables.  Select regions you would
 % like to fit to steps, and play with the fitting.  Once you are satisfied
 % with the results, save the figures and step fitting data.  Either now or
 % later, you can further organize the data into matrices by setting
 % data_organize_option to 1.  Be sure to save this data as well.


datadirectory = 'C:\Users\seanc\Documents\labelled UvrD stepping\dimer step fits\5 uM data\111122 file 74 fits\';
% 
param.Date = '111122'; param.datafnumber = 74; % Initialize place where data and figures will
                                               % be saved, and other essential info about time trace
save_data = 0;                                   

save_figures = 0;     % Important options to consider when evaluating steps

dwell_time_op = 1;

pairwise = 1;

data_organize = 0;

fig = gcf;
message = msgbox({['First, select beginning and end of step trace.'] ['Then, press return.']});
selectedData = datacursormode(fig);
pause; datacursormode off            % open figure, then select the region to fit 
vals = getCursorInfo(selectedData);  % to step fitting algorithm using the data cursor

indx_steps = [];
    for i = 1:1:length(vals)
        indx_steps = [indx_steps vals(i).DataIndex];
    end   
    indx_steps = sort(indx_steps);

 steps_time = time(indx_steps(1):indx_steps(2));  % changed from trapData.time to just time on 10/31/2018

 steps_nbp = nbp(indx_steps(1):indx_steps(2));    % changed from trapData.nbp to just nbp on 10/31/2018

 steps_force = mean(FavgX(indx_steps(1):indx_steps(2))); % need to have processed data for time trace loaded into the MATLAB workspace

 steps_force_av = FmeanX;

 steps_find_input_data = [steps_time',steps_nbp'];

[data, indexes, lijst, properties,initval] = Steps_Find(length(steps_find_input_data),steps_find_input_data,1); % fit the steps

[Steppedness,Steps_all,Fit] = Steps_Evaluate(data, indexes,lijst, properties,initval,5,1); % Need to choose number of steps (second to last input) each time


if save_figures == 1

    fpath_save = [datadirectory '\processed data and figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' Step Fitting Results Interval 1.fig'];
    saveas(gca, fullfile(fpath_save, name_save));

end                                      % If satisfied with fit, should save figures

if pairwise == 1
    
    Dist = pdist(steps_nbp');            % pairwise distance is a good check for
                                         % steppy behavior
    [a, b, c] = histcounts(Dist);            
     
    z = (b(2:end)+b(1:end-1))/2;
    
    figure
    
    hold on
    
    plot(z,a)
    
    ylabel('Counts')
    
    xlabel('Distance (basepairs)')
    
    title({['Pairwise Distribution (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] ' Interval 1'});
    
    if save_figures == 1;

        fpath_save = [datadirectory '\processed data and figures\'];
        name_save = ['File ' num2str(param.datafnumber,'%03d') ' Pairwise Distribution Interval 1.fig'];
        saveas(gca, fullfile(fpath_save, name_save));

    end      % save pairwise distance figure

    
end


if dwell_time_op == 1;
    
      [x, y] = unique(Fit,'stable');
     
      yy = y(2:end);                        % should always calculate 
                                            % dwell times too
      dwell_time = [];

      for ii = length(yy):-1:2;
            D = steps_time(1,yy(ii)) - steps_time(1,yy(ii-1)) - 0.01;
            dwell_time = [dwell_time; D];
      end
        
end
    


figure

plot(steps_time, steps_nbp);

hold on
                                % saves just the plot of fitted steps
plot(steps_time, Fit, 'r');

xlabel('time(s)')

ylabel('basepairs unwound')

title({['Fitted Steps (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] ' Interval 1'});

if save_figures == 1

fpath_save = [datadirectory '\processed data and figures\'];
name_save = ['File ' num2str(param.datafnumber,'%03d') '  Reconstructed Steps Interval 1.fig'];
saveas(gca, fullfile(fpath_save, name_save));

end

if save_data == 1
    
step_data.step_num = 4;  % will vary each time, as input parameters for Steps_Find (factor)
                         % and Steps_Evaluate (step_num), should save 
step_data.factor = 1;    % in case re-fitting is ever needed
    
step_data.steps_time = steps_time;

step_data.steps_nbp = steps_nbp;      %re-name variables for saving

step_data.steps_force = steps_force;

step_data.steps_force_av = steps_force_av;

step_data.indx_steps = indx_steps;

step_data.Steppedness = Steppedness;

step_data.Steps_all = Steps_all;

step_data.Fit = Fit;

step_data.Dist = Dist;

step_data.a = a;

step_data.b = b;

step_data.c = c;

step_data.z = z;

step_data.x = x;

step_data.y = y;

step_data.dwell_time = dwell_time;

save_path = [datadirectory '\processed data and figures\']; % saves basic stepping data

cd(save_path)

save([param.Date '_' num2str(param.datafnumber,'%03d') ' Stepping Data Interval 1.mat'], '-struct', 'step_data');

end

if data_organize == 1   % further organizes data, it is recommended to do this at some point
                        % data is ultimatley compiled into a matrix
                        % with all relevant info for fitted step sizes 
                        % (i.e., force, DNA sequence position, etc.)
                     
    date = 190920; filenum = 110; intnum = 7;   % Be sure to change these with each passing round

    pos_cor = 0.0;  
    
    stepdat = Steps_all; dwelldat = dwell_time;             
     
    xdat = x; Fitdat = Fit;
    
    step_test = transpose(stepdat);

    steps_tot = step_test(:,1);                  % organizes pdist data
    
    [pwd.dist_tot, pwd.dist_pos, pwd.dist_neg] = p_dist(steps_nbp');

    [step_sem, dwell_sem, sem, step_std, dwell_std, stdev] = Step_Error_Calc(steps_nbp, xdat, Fitdat); %sometimes change from nbp to steps_nbp

    dwell_sem_v2 = sem(2:end - 1);                              % calculates standard deviationa and standard error for each step
 
    dwell_std_v2 = stdev(2:end - 1);

    date_vec = date*ones(length(steps_tot),1);

    fnum_vec = filenum*ones(length(steps_tot),1);

    Int_vec = intnum*ones(length(steps_tot),1);

    force_step = steps_force*ones(length(steps_tot),1);

    force_av_step = steps_force_av*ones(length(steps_tot),1);

    start_st = indx_steps(1)*ones(length(steps_tot),1);

    end_st = indx_steps(2)*ones(length(steps_tot),1);

    step_pos = xdat(1:end - 1) - pos_cor;  

    step_mat = horzcat(date_vec, fnum_vec, Int_vec, start_st, end_st, steps_tot, force_step, force_av_step, step_pos, step_sem, step_std);
                                         % stepping data matrix
    dwells_rev = flip(dwelldat);
    
    dwell_pos = xdat(2:end - 1) - pos_cor;

    start_dw = indx_steps(1)*ones(length(dwells_rev),1);

    end_dw = indx_steps(2)*ones(length(dwells_rev),1);

    step_cor = steps_tot(2:end,1);

    step_sem_v2 = step_sem(2:end,1);

    step_std_v2 = step_std(2:end,1);

    date_vec_d = date*ones(length(dwells_rev),1);

    fnum_vec_d = filenum*ones(length(dwells_rev),1);

    Int_vec_d = intnum*ones(length(dwells_rev),1);

    force_dwell = steps_force*ones(length(dwells_rev),1);

    force_av_dwell = steps_force_av*ones(length(dwells_rev),1);

    dwell_mat = horzcat(date_vec_d, fnum_vec_d, Int_vec_d, start_dw, end_dw, dwells_rev, step_cor, step_std_v2, step_sem_v2, force_dwell, force_av_dwell, dwell_pos, dwell_sem_v2, dwell_std_v2);
                                      % dwell data matrix
    % separate dwells by type (i.e., +/+,-/-,+/-,-/+)

    ff_dwell = [];

    bb_dwell = [];

    fb_dwell = [];

    bf_dwell = [];

    for i = 1:1:length(dwells_rev)
    
        if steps_tot(i) > 0 && steps_tot(i+1) > 0 % (+/+)
            ff = dwell_mat(i,:);
            ff_dwell = [ff_dwell; ff];
        end 
        
        if steps_tot(i) < 0 && steps_tot(i+1) < 0  % (-/-)
            bb = dwell_mat(i,:);
            bb_dwell = [bb_dwell; bb];
        end     
    
        if steps_tot(i) > 0 && steps_tot(i+1) < 0  % (+/-)
            fb = dwell_mat(i,:);
            fb_dwell = [fb_dwell; fb];
        end     

        if steps_tot(i) < 0 && steps_tot(i+1) > 0  % (-/+)
            bf = dwell_mat(i,:);
            bf_dwell = [bf_dwell; bf];
        end     

    end 
    
    step_data_final.pdist = pwd;
    
    step_data_final.step_mat = step_mat;
    
    step_data_final.dwell_mat = dwell_mat;
    
    step_data_final.ff_dwell = ff_dwell;  % final organized groups of dwells by type
                                          % will also be matrices
    step_data_final.bb_dwell = bb_dwell;
    
    step_data_final.fb_dwell = fb_dwell;
    
    step_data_final.bf_dwell = bf_dwell;
     
    save_path = [datadirectory '\processed data and figures\'];
   % save_path = 'E:\2019 p5 uM data\other intervals\';          
  
    cd(save_path)

    save([param.Date '_' num2str(param.datafnumber,'%03d') 'Complete Stepping Data Interval 7.mat'], '-struct', 'step_data_final');    
    
end
                      % saves organized stepping data


