function [ff_dwell, bb_dwell, fb_dwell, bf_dwell] = dwellsort_new_step_data(stepmat, dwellmat)
% Function designed to sort dwells according to step pair types for
% reanalyzed step and dwell data output from Step_data_recalculate, 
% accepts step and dwell matrices as inputs (need to put in both)

[~, ind_step] = unique(stepmat(:,1:4),'rows');  % figure out indices of unique step fitting intervals

% [~, ind_dwell] = unique(dwellmat(:,1:4),'rows');

ind_step = sort(ind_step);

% ind_dwell = sort(ind_dwell);

a = length(ind_step);

ff_dwell = []; bb_dwell = []; fb_dwell = []; bf_dwell = [];

for i = 1:1:(a - 1)
    
    int_step = stepmat((ind_step(i):((ind_step(i+1)-1))),:); % identify dwells and steps for appropriate fitted interval
    
    int_start_time = int_step(1,3);
    
    dwell_vec_ind = find(dwellmat(:,3) == int_start_time);
    
    int_dwell = dwellmat(dwell_vec_ind,:);
    
    b = length(int_step(:,5));
    
    for j = 1:1:(b-1)  % classify dwells as +/+,-/-,+/-, and -/+
        
        if int_step(j,5) > 0 && int_step(j+1,5) > 0  % +/+
            ff = int_dwell(j,:);   
            ff_dwell = [ff_dwell; ff];            
        end
            
        if int_step(j,5) < 0 && int_step(j+1,5) < 0  % -/- 
            bb = int_dwell(j,:);   
            bb_dwell = [bb_dwell; bb];     
        end
        
        if int_step(j,5) > 0 && int_step(j+1,5) < 0  % +/-
            fb = int_dwell(j,:);   
            fb_dwell = [fb_dwell; fb];
        end
    
        if int_step(j,5) < 0 && int_step(j+1,5) > 0  % -/+
            bf = int_dwell(j,:);   
            bf_dwell = [bf_dwell; bf];
        end
        
    end
    
end
    
    int_step_end = stepmat(ind_step(end):end,:);  % classify dwell types for last step interval
    
    int_start_time_end = int_step_end(1,3);

    dwell_vec_ind_end = find(dwellmat(:,3) == int_start_time_end);
    
    int_dwell_end = dwellmat(dwell_vec_ind_end,:);

    c = length(int_step_end(:,5));
    
    for k = 1:1:(c - 1)  % again, sort by step pairs
        
        if int_step_end(k,5) > 0 && int_step_end(k+1,5) > 0  % +/+
            ff_end = int_dwell_end(k,:);   
            ff_dwell = [ff_dwell; ff_end];
        end
        
        if int_step_end(k,5) < 0 && int_step_end(k+1,5) < 0 % -/-
            bb_end = int_dwell_end(k,:);   
            bb_dwell = [bb_dwell; bb_end];
        end
        
        if int_step_end(k,5) > 0 && int_step_end(k+1,5) < 0 % +/-
            fb_end = int_dwell_end(k,:);   
            fb_dwell = [fb_dwell; fb_end];
        end
        
        if int_step_end(k,5) < 0 && int_step_end(k+1,5) > 0 % -/+
            bf_end = int_dwell_end(k,:);   
            bf_dwell = [bf_dwell; bf_end];
        end
        
    end
    

end

