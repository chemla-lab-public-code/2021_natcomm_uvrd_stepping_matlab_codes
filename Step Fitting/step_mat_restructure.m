function [step_mat_v2] = step_mat_restructure(step_mat)
% Short functin to restructure step size matrices for SPC data
% in new matrix, order of columns are as follows:
% 1. date 2. filenum 3. start 4. end 5. step 6. force 7. pos before step
% 8. step stdev  9.  step sem

date = step_mat(:,1);

filenum = step_mat(:,2);

start = step_mat(:,4);

stop = step_mat(:,5);

step = step_mat(:,6);

force = step_mat(:,7);

pos = step_mat(:,9);

step_std = step_mat(:,11);

step_sem = step_mat(:,10);

step_mat_v2 = horzcat(date, filenum, start, stop, step, force, pos, step_std, step_sem);

end

