% Author: Kevin Whitley
% Date created: 170622

% 1D kernel density plot. Input the data vector, standard deviation of
% mini-gaussians (sig). The bin width and range are for plotting purposes.
% Used mostly for  recalculating step locations in fitted steps (to represent 
% the most probable position in the dwells flanking the steps rather than 
% the mean), so, for convenience, it does not plot each kernel density
% estimate.

function [kerns,tv] = kernel_density_1D(data, sig, binwid, range)

pgauss = [];
tv = range(1):binwid:range(2); % test variable

for gg = 1:length(data)
    pgauss(:,gg) = 1/(sig(gg)*sqrt(2*pi)).*exp(-(tv'-data(gg)).^2/(2*sig(gg)^2));
end

kerns = sum(pgauss,2);

% plot(tv, kerns/sum(kerns)/binwid, 'linew', 2)