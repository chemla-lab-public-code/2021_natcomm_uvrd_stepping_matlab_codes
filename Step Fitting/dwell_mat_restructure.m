function [dwell_mat_2] = dwell_mat_restructure(dwell_mat)
% short function to slightly reorganize columns for dwell time
% data into final form

% order of matrices is as follows:  1. date 2. filename 3. start 4. end  5.
% dwell 6. step after dwell 7. force 8. pos 9. step stdev 10.  step sem
% 11. dwell stdev 12. dwell sem

date = dwell_mat(:,1);

filenum = dwell_mat(:,2);

start_dw = dwell_mat(:,4);

end_dw = dwell_mat(:,5);

dwell = dwell_mat(:,6);

step = dwell_mat(:,7);

force = dwell_mat(:,10);

pos = dwell_mat(:,12);

step_std = dwell_mat(:,8);

step_sem = dwell_mat(:,9);

dwell_std = dwell_mat(:,14);

dwell_sem = dwell_mat(:,13);

dwell_mat_2 = horzcat(date, filenum, start_dw, end_dw, dwell, step, force, pos, step_std, step_sem, dwell_std, dwell_sem);


end

