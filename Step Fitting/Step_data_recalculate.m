% Script to re-process step fit data, re-calculating the step locations as
% the most likely positions in the dwells flanking the steps, rathern the
% mean postions.  Uses kernel density estimation with a gaussian kernel to
% calculate most likely position in each dwell.  To run, first edit key
% parameters and names of matrices below.  Then, click run, and when
% prompted, select the folder containing all of the .fig files for the step
% fits in question.  Select all of the .fig files and hit enter.

% ============= Options =============
loadfile = 1; % if 1 loads files and extracts data, if 0 assumes data is already loaded
plotoption = 1; % if 1 plots results

% ============= Load files =============
if loadfile == 1
    [filename,pathname]=uigetfile('*.*','Open fig files to analyze:','MultiSelect','on');
    data = cell(size(filename)); stepfit = cell(size(filename)); 
    start_time = cell(size(filename)); final_time = cell(size(filename)); % added by SPC on 10/6/2020 to get start/end times
    time_all = cell(size(filename)); % added on 10/16/2020 by SPC
    clear bw
    % Loads multiple figure files and extracts the data plotted
    for i = 1:length(filename)
        fn = [pathname filename{i}];
        uiopen(fn,1)
        [x,y] = Extractdata;
        data{i} = y{1};
        stepfit{i} = y{2};
        time_all{i} = x{1}; % added by SPC on 10/16/2020
        start_time{i} = x{1}(1);   % lines 18 and 19 added on 10/6/2020 to get start and end times
        final_time{i} = x{1}(end);
        bw(i) = 1/(x{1}(2)-x{1}(1));
    end;
end;    

% ============= Define parameters =============
bw0 = 100; % if data is collected at bw > bw0, data are filtered
crop_dur = 0.019; % crop all dwells shorter than this duration, 0.019 for old trap, 0.007 for fleezers
sig = 0.1; % width of Gaussian kernel 
range = 0:0.01:30; % KDE range in bp, can be adjusted

% set up and initialize data for what will go into dwell and step matrices
mean_dwell = cell(size(filename)); std_dwell = cell(size(filename)); sem_dwell = cell(size(filename)); num_points = cell(size(filename));
kd_data = cell(size(filename)); kd_fit = cell(size(filename)); step = cell(size(filename)); step_sem = cell(size(filename)); 
step_std = cell(size(filename)); % step_std added on 10/6/2020 by SPC
step_pos = cell(size(filename));  % step_pos (position after step) added on 10/6/2020 by SPC
dwell_dur_crop_compile = cell(size(filename));% added by SPC on 10/6/2020
dwell_time = cell(size(filename));
std_dwell_corr = cell(size(filename)); % added by SPC on 10/6/2020
sem_dwell_corr = cell(size(filename)); % added by SPC on 10/6/2020
dwell_pos = cell(size(filename)); % gets positions of dwells, added by SPC on 10/6/2020
dwell_step = cell(size(filename)); % gets size of the step occuring after each dwell, added by SPC on 10/6/2020
dwell_step_std = cell(size(filename)); % gets std errors for dwell step, added by SPC on 10/6/2020
dwell_step_sem = cell(size(filename)); % gets sem errors for dwell sem, added by SPC on 10/6/2020
step_start_int_time = cell(size(filename)); % added by SPC on 10/13/2020
step_end_int_time = cell(size(filename));   % added by SPC on 10/13/2020
dwell_start_int_time = cell(size(filename)); % added by SPC on 10/13/2020
dwell_end_int_time = cell(size(filename));   % added by SPC on 10/13/2020
dwell_dur_v2_crop_compile = cell(size(filename)); % added by SPC on 10/16/2020
pdist_tot = cell(size(filename));  % pdist stuff added by SPC on 12/18/2020
pdist_pos = cell(size(filename));
pdist_neg = cell(size(filename));

for i = 1:length(filename)
    clear dwell_start dwell_end
    idx = find(abs(diff(stepfit{i})) > 0); % find step transitions in figures
    dwell_start = [1 idx+1];
    dwell_end = [idx length(stepfit{i})];       
    dwell_dur = (dwell_end-dwell_start)/bw(i); 
    
    crop_idx = find(dwell_dur > crop_dur); % crop out short dwells
    dwell_start_crop = dwell_start(crop_idx);
    dwell_end_crop = dwell_end(crop_idx);
    dwell_dur_crop = dwell_dur(crop_idx); % added by SPC on 10/6/2020
    dwell_dur_crop_compile{i} = dwell_dur_crop;  % added by SPC on 10/6/2020
    
  
    for j = 1:length(dwell_start_crop)
        datawin0 = data{i}(dwell_start_crop(j):dwell_end_crop(j));
        if bw(i) > bw0
            datawin = FilterAndDecimate(datawin0,round(bw(i)/bw0));
        else
            datawin = datawin0;
        end;    
        [kdtest,postest] = kernel_density_1D(datawin, sig*ones(size(datawin)), 0.01, [min(datawin)-sig max(datawin)+sig]);
        mean_dwell{i}(j) = postest(find(kdtest == max(kdtest))); % dwell at most probable position from KDE
        % mean_dwell{i}(j) = mean(datawin); % dwell at mean position 
        std_dwell{i}(j) = sqrt(sum((datawin - mean_dwell{i}(j)).^2)/(length(datawin)-1));
        % std_dwell{i}(j) = std(datawin);   
        sem_dwell{i}(j) = std_dwell{i}(j)/sqrt(length(datawin));
        num_points{i}(j) = length(datawin);
    end;
    
    kd_data{i} = kernel_density_1D(data{i}, sig*ones(size(data{i})), range(2)-range(1), [range(1) range(end)]);
    kd_fit{i} = kernel_density_1D(mean_dwell{i}, sem_dwell{i}, range(2)-range(1), [range(1) range(end)]);
    
    step{i} = diff(mean_dwell{i});
    step_sem{i} = sqrt(sem_dwell{i}(1:end-1).^2+sem_dwell{i}(2:end).^2);
    step_std{i} = sqrt(std_dwell{i}(1:end-1).^2+std_dwell{i}(2:end).^2); % step_std added on 10/6/2020 by SPC
    step_pos{i} = mean_dwell{i}(1:end-1); % position after step, added by SPC on 10/6/2020
    dwell_time{i} = dwell_dur_crop_compile{i}(2:end - 1); % gets accurate dwell times, added by SPC on 10/6/2020, removes first and last dwells, which aren't real
    std_dwell_corr{i} = std_dwell{i}(2:end-1); % removes first and last std, which do not correspond to real dwells, added by SPC on 10/6/2020
    sem_dwell_corr{i} = sem_dwell{i}(2:end-1); % removes first and last sem, which do not correspond to real dwells, added by SPC on 10/6/2020
    dwell_pos{i} = mean_dwell{i}(2:end-1); % gets dwell position, added by SPC on 10/6/2020
    dwell_step{i} = step{i}(2:end); % gets size of step occuring after each dwell, added by SPC on 10/6/2020
    dwell_step_std{i} = step_std{i}(2:end);
    dwell_step_sem{i} = step_sem{i}(2:end);
    step_start_int_time{i} = start_time{i}*ones(1,(length(step{i})));
    step_end_int_time{i} = final_time{i}*ones(1,(length(step{i})));
    dwell_start_int_time{i} = start_time{i}*ones(1,(length(dwell_time{i})));
    dwell_end_int_time{i} = final_time{i}*ones(1,(length(dwell_time{i})));
    [pdist_tot{i}, pdist_pos{i}, pdist_neg{i}] = p_dist(data{i}); % calculates pairwise distance data
    
    
    % Now, to organize everything into matrices for convenience
    
    step_vec = cell2mat(step)';
    step_pos_vec = cell2mat(step_pos)';  % step size matrix
    step_std_vec = cell2mat(step_std)';
    step_sem_vec = cell2mat(step_sem)';
    step_start_int_time_vec = cell2mat(step_start_int_time)';
    step_end_int_time_vec = cell2mat(step_end_int_time)';
    step_force_vec = zeros(length(step_vec),1);
    step_date_vec = zeros(length(step_vec),1);
    step_filenum_vec = zeros(length(step_vec),1);
    stepmat = horzcat(step_date_vec, step_filenum_vec, step_start_int_time_vec, step_end_int_time_vec, step_vec, step_force_vec, step_pos_vec, step_std_vec, step_sem_vec);
    % May change name of matrix depending on the data set
    
    dwell_time_vec = cell2mat(dwell_time)'; % dwell time matrix
    dwell_pos_vec = cell2mat(dwell_pos)';
    dwell_std_vec = cell2mat(std_dwell_corr)';
    dwell_sem_vec = cell2mat(sem_dwell_corr)';
    dwell_step_vec = cell2mat(dwell_step)';
    dwell_step_std_vec = cell2mat(dwell_step_std)';
    dwell_step_sem_vec = cell2mat(dwell_step_sem)';
    dwell_start_int_time_vec = cell2mat(dwell_start_int_time)';
    dwell_end_int_time_vec = cell2mat(dwell_end_int_time)';
    dwell_force_vec = zeros(length(dwell_time_vec),1);
    dwell_date_vec = zeros(length(dwell_time_vec),1);
    dwell_filenum_vec = zeros(length(dwell_time_vec),1);
    dwellmat = horzcat(dwell_date_vec, dwell_filenum_vec, dwell_start_int_time_vec, dwell_end_int_time_vec, dwell_time_vec, dwell_step_vec, dwell_force_vec, dwell_pos_vec, dwell_step_std_vec, dwell_step_sem_vec, dwell_std_vec, dwell_sem_vec);
    % may change name of matrix depending on the dataset
    
    pdist_tot_vec = cell2mat(pdist_tot');  % get pairwise distance vectors
    pdist_pos_vec = cell2mat(pdist_pos');  % may change the names of these
    pdist_neg_vec = cell2mat(pdist_neg');  % to describe specific dataset
    
    if plotoption == 1
        if i == 1         % plots one figure with all of the reconfigured step 
            figure;       % together and then plots another one with all of 
            h = gcf;      % the kernel density estimates of dwell position at each
        else              % at each dwell in each step fitting interval
            figure(h);
        end;    
        subplot(1,2,1);
        plot((0:(length(data{i})-1))/bw(i),data{i},'b','DisplayName',filename{i}); hold on;
        stairs([dwell_start_crop(1) dwell_end_crop]/bw(i),[mean_dwell{i} mean_dwell{i}(end)],'r');       
        subplot(1,2,2);
        plot(kd_data{i}/sum(kd_data{i}),range,'b','DisplayName',filename{i}); hold on;
        plot(kd_fit{i}/sum(kd_fit{i}),range,'r');
    end;
end;

% figure;
% subplot(2,1,1);
% kdtot = zeros(size(range));
% for i = 1:length(filename)
%     plot(range,kd_data{i}); hold on;
%     kdtot = kdtot + kd_data{i}';
%     %title('Select x axis start point');
%     %xp{i} = ginput(1);
% end;    
% plot(range,kdtot,'k--');
% 
% subplot(2,1,2);
% kdtot = zeros(size(range));
% for i = 1:length(filename)
%     plot(range,kd_fit{i},'r'); hold on;
%     kdtot = kdtot + kd_fit{i}';
%     %title('Select x axis start point');
%     %xp{i} = ginput(1);
% end;    
% plot(range,kdtot,'k--');
