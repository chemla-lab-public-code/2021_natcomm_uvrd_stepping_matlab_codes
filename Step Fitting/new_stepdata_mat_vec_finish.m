function [date_vec,filename_vec,force_vec] = new_stepdata_mat_vec_finish(old_stepmat,new_stepmat)
% Function to fill in columns for date, force, and file name on new step
% and dwell matrices output from Step_data_recalculate, since these
% do not have those columns filled in since it is impossible to extract
% them from the .fig files of fitted steps for Step_data_recalculate.
% Instead, these values are extracted from the older step and dwell
% matrices.  This code can be used for both step and dwell matrices.

% date_vec = [];
% filename_vec = [];
% force_vec = [];

steps = new_stepmat(:,5);

date_vec = zeros(length(steps),1);     % Initialize variables
filename_vec = zeros(length(steps),1);
force_vec = zeros(length(steps),1);

start_time_vec = new_stepmat(:,3);  % Set up to fill in new matrices by comparing start times between old and new

   oldmat_start_time = round(old_stepmat(:,3));

%    oldmat_start_time = round((old_stepmat(:,3)*100) + 1); % alternate for when dealing with different bandwidth in other data occasionally

% start_time_vec_corr = round((start_time_vec*100) + 1);

start_time_vec_corr = round(start_time_vec); % added by SPC on 10/20/2020



[~, ind] = unique(start_time_vec_corr);

ind = sort(ind);

for j = 1:1:(length(ind)-1)
    
    start_time_temp = start_time_vec_corr(ind(j));
    
%     time_temp_vec = start_time_vec_corr(ind(j):(ind(j+1) - 1));
    
    match_ind = find(oldmat_start_time == start_time_temp);  % find indices where start time for step intervals are the same between old and new matrices
    
    match_ind = sort(match_ind);
    
    oldmat_sect_temp = old_stepmat(match_ind,:);
    
    date_temp = oldmat_sect_temp(1,1);
    
    filename_temp = oldmat_sect_temp(1,2);
    
    force_temp = oldmat_sect_temp(1,6);
    
%     date_temp_vec = date_temp*ones(length(time_temp_vec),1);
%     
%     filename_temp_vec = filename_temp*ones(length(time_temp_vec),1);
%     
%     force_temp_vec = force_temp*ones(length(time_temp_vec),1);
% 
%     date_vec = [date_vec; date_temp_vec];
%     
%     filename_vec = [filename_vec; filename_temp_vec];
%     
%     force_vec = [force_vec; force_temp_vec];
    
    date_vec(ind(j):(ind(j+1)-1)) = date_temp;  % fill in new matrices with appropriate values for each stepping interval
    
    filename_vec(ind(j):(ind(j+1)-1)) = filename_temp;

    force_vec(ind(j):(ind(j+1)-1)) = force_temp;
    
end

start_time_last = start_time_vec_corr(ind(end));

% time_last_vec = start_time_vec_corr(ind(end):end);

match_ind_last = find(oldmat_start_time == start_time_last);

match_ind_last = sort(match_ind_last);

oldmat_sect_last = old_stepmat(match_ind_last,:);

date_last = oldmat_sect_last(1,1);  

filename_last = oldmat_sect_last(1,2);

force_last = oldmat_sect_last(1,6);

% date_last_vec = date_last*ones(length(time_last_vec),1);
% 
% filename_last_vec = filename_last*ones(length(time_last_vec),1);
% 
% force_last_vec = force_last*ones(length(time_last_vec),1);
% 
% date_vec = [date_vec; date_last_vec];
%     
% filename_vec = [filename_vec; filename_last_vec];
%     
% force_vec = [force_vec; force_last_vec];

date_vec(ind(end):end) = date_last;
    
filename_vec(ind(end):end) = filename_last; % fill in values for last interval

force_vec(ind(end):end) = force_last;


end

