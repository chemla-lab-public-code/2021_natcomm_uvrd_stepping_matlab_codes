function [step_sem, dwell_sem, sem, step_std, dwell_std, stdev] = Step_error_calc(data, indx, Fit)
% Function to calculate errors for each step and dwell for UvrD stepping
% analysis.  Error on dwell is just standard error of mean.  Error on steps
% is square root of the sum of the squares of the standard errors of the
% dwells before and after.  Is called in Steps_Analyze_Host_code


average = [];
stdev = [];
sem = [];
step_std = [];
step_sem = [];

k = length(indx);

for i = 1:1:k
    intfin = find(Fit == indx(i));
    int = data(intfin);
    av_time = mean(int);
    std_time = std(int);
    sem_time = (std_time)/sqrt(length(int));
    
    average = [average; av_time];
    stdev = [stdev; std_time];
    sem = [sem; sem_time];
    
end
    

%for i = 1:1:(k - 1);
    
%     intfind = data((indx(i)):(indx(i + 1) - 1));
%     av_time = mean(int);
%     std_time = std(int);
%     sem_time = (std_time)/sqrt(length(int));
%     
%     average = [average; av_time];
%     stdev = [stdev; std_time];
%     std_er = [std_er; sem_time];
    
% end

% intf = data(279:391);
%      av_time_f = mean(intf);
%      std_time_f = std(intf);
%      sem_time_f = (std_time)/sqrt(length(intf));
%      
% average_all = [average; av_time_f];
% stdev_all = [stdev; std_time_f];
% std_er_all = [std_er; sem_time_f];


for j = 1:1:(k-1)
    step_sem_sum = sqrt((sem(j))^2 + (sem(j+1))^2);
    step_sem = [step_sem; step_sem_sum];
    step_std_sum = sqrt((stdev(j))^2 + (stdev(j+1))^2);
    step_std = [step_std; step_std_sum];
end

dwell_sem = flip(sem(2:end - 1));
dwell_std = flip(stdev(2:end - 1));


end

