function x = FilterAndDecimate(xd, num);
% Short function to filter/downsample time trace data, specifically used
% with Step_data_recalculate script to filter step fit data

x = filter(ones(1,num)/num, 1, xd);
idx = num:num:length(xd);

x = x(idx);