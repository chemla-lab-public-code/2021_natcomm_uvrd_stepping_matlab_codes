% Script to create additional, optional plots when converting time traces
% from nm to bp unwound.  Will plot total DNA extesion in nm vs time, 
% DNA extension change (in nm vs time), force vs time, and bp unwound 
% vs time, all as separate plots.

if param.plot_other == 1
    
    if param.direction == 'Y'
        trapData.Fmean = trapData.FmeanY;    % plot change in DNA extension change in nm vs time
    end
    if param.direction == 'X'
        trapData.Fmean = trapData.FmeanX;
    end
    
    figure
    if param.direction == 'Y'
        plot(trapData.time,trapData.DNAext_Y)
    end
    if param.direction == 'X'
        plot(trapData.time,trapData.DNAext_X)
    end
    xlabel('time(s)')
    ylabel('DNA Extension Change (nm)')
    title({['DNA Extension Change vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.Fmean) ' pN']});
    %savefig(['File ' num2str(param.datafnumber,'%03d') ' extension change only updated'])
    fpath_save = [datadirectory '\other figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' DNA extension change.fig'];
    saveas(gca, fullfile(fpath_save, name_save));
    close
    
    figure
    if param.direction == 'Y'                      % plot total DNA extension in nm vs time
        plot(trapData.time,trapData.DNAext_Ytot)
    end
    if param.direction == 'X'
        plot(trapData.time,trapData.DNAext_Xtot)
    end
    xlabel('time(s)')
    ylabel('Total DNA Extension(nm)')
    title({[' Total DNA Extension vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.Fmean) ' pN']});
    %savefig(['File ' num2str(param.datafnumber,'%03d') ' total DNA extension only updated'])
    fpath_save = [datadirectory '\other figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' total DNA extension.fig'];
    saveas(gca, fullfile(fpath_save, name_save));
    close
    
    figure
    if param.direction == 'Y'
        plot(trapData.time, trapData.force_AY) % plot forces vs time
        hold on
        plot(trapData.time, trapData.force_BY)
    end 
    if param.direction == 'X'
        plot(trapData.time, trapData.force_AX)
        hold on
        plot(trapData.time, trapData.force_BX)
    end
    ylabel('Force (pN)')
    xlabel('Time (s)')
    if param.direction == 'X'
        legend('AX','BX')
    end
    if param.direction == 'Y'
        legend('AY','BY')
    end
    title({['Forces vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.Fmean) ' pN']});
    %savefig(['File ' num2str(param.datafnumber,'%03d') ' forces only updated'])
    fpath_save = [datadirectory '\other figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' both forces.fig'];
    saveas(gca, fullfile(fpath_save, name_save));
    close
    
    figure
    if param.direction == 'Y'
        plot(trapData.time, trapData.FavgY)
    end
    if param.direction == 'X'
        plot(trapData.time, trapData.FavgX)
    end 
    xlabel('time(s)')
    ylabel('average force')
    if param.direction == 'Y'
        legend('in Y')
    end
    if param.direction == 'X'
        legend('in X')
    end
    title({['Average Force vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.Fmean) ' pN']});   
    %savefig(['File ' num2str(param.datafnumber,'%03d') ' average force only updated'])
    fpath_save = [datadirectory '\other figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' average force.fig'];
    saveas(gca, fullfile(fpath_save, name_save));
    close    
    
    figure
    plot(trapData.time, trapData.nbp)  % plot bp unwound vs time in single plot
    xlabel('time(s)')
    ylabel('Basepairs Unwound')
    title({['Basepairs Unwound vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.Fmean) ' pN']});
    %savefig(['File ' num2str(param.datafnumber,'%03d') ' basepairs unwound only updated'])
    fpath_save = [datadirectory '\processed data and figures\'];
    name_save = ['File ' num2str(param.datafnumber,'%03d') ' base pairs unwound.fig'];
    saveas(gca, fullfile(fpath_save, name_save));
    close
    
end