function [F_av,xtot,Gtot] = ModelHairpinUnwind(Seq_num,loop,ssbs)
% Created July 11, 2017 - BKS
% Calculates free energy and theoretical model of hairpin pulling curve
% Calls XWLCCountor and SeqFreeEnergy2. Adapated from ModelHairpinUnwindKW
% to include an ssDNA loading site in pulling curve calculation
% 
% Inputs: Seq_num  -  either a number corresponding to a saved sequence
% (below in case structure) or the full 5'->3' sequence of your choice.
% loop  -  hairpin loop sequence
% ssbs  -  length of single stranded binding/loading site




% Input hairpin sequence
if nargin <2
    loop = 'TTTT';
    ssbs = 10; %10nt binding site
end
if nargin == 0
Seq_num = 1;
end

if length(Seq_num) ==1
    switch Seq_num
        case 1 % Zhi's HP1
            seq = 'GGC TGA TAG CTG AGC GGT CGG TAT TTC AAA AGT CAA CGT ACT GAT CAC GCT GGA TCC TAG AGT CAA CGT ACT GAT CAC GCT GGA TCC TA';
        case 2
            seq = 'AAT CAG CGA TCA GAT AAC TAA CGC CCT GGG GAC TGG TAC GTC AGC TGT ATC AAG CTT CGA GAC TGG TAC GTC AGC TGT ATC AAG CTT CG';
        case 3
            seq = 'AAT CAT CGA TAA TAT AAC TAA TGC ATC GAA AAT CAG TGA AAA TCA GCT ACA ACG CCC TGG GGA CTG GTA CGT CAG CTG TAT CAA GCT TA';
        case 4 % BKS 1.0
            seq = 'TCT CTC AGT CTG AGA TGT CAG TCT CAG TCA GAG TCT TGT CTG TGT CTT GTT GAT GTC ACT GAC TGA GAC TCT GAC TGT CTG GGT CGC GC';
        case 5 % BKS 1.1
            seq = 'AG TCT CAG TCA CTC ATG TCA GTC ACA GTC AGA GTC ATG TCT GAG TCT TGA TGA TGT CAC TGA CTG AGA CTC TGA CTC ACT GAG TCG AGC';
    end
else
    seq = Seq_num;
end

idx = findstr(seq,' '); seq = seq(setxor(1:length(seq),idx)); %remove spaces

% Constants
kT = 4.14; %[pN nm]

% Params
ktrap = 0.26; %[pN/nm]
lds = 3.055e3; %2e3; %20; %DNA handle total length [bp]
lss = length(seq)+length(loop)/2; %hairpin length [bp]  % SPC 12/3/2018  - Why would length of loop be divided by 2?

% WLC parameters
[WLC_param] = WLC_parameters;
hds = WLC_param.hds; % [nm/bp] interphosphate distance
hss = WLC_param.hss; % nm/nt
Pds = WLC_param.Pds; % nm
Pss = WLC_param.Pss; % nm
Sss = WLC_param.Sss; % pN
Sds = WLC_param.Sds; % pN
kT = WLC_param.kT;% Zhi's values (dsDNA)


Fmax = 20;
xssmax = (2*lss + ssbs)*hss*XWLCContour(Fmax,Pss,Sss);
xdsmax = lds*hds*XWLCContour(Fmax,Pds,Sds);
xbmax = Fmax/ktrap;
xtotmax = xbmax + xdsmax + xssmax;  

dx = 1; % nm    10^(round(log10(xtotmax))-2);
xtot = 900:dx:xtotmax;

clear F
F = nan(length(xtot),lss+1);
for i = 1:lss+1
    for j = 1:length(xtot)
        F(j,i) = fzero(@(y) (2*(i-1)+ssbs)*hss*XWLCContour(y,Pss,Sss)+lds*hds*XWLCContour(y,Pds,Sds) + y/ktrap - xtot(j), 1);
    end
end

% Unwinding free energy
% dG = SeqFreeEnergy3(seq,loop,'FR', 0.02+0.05, 0.003)*kT; %[pN nm]  % Kevin's salt correction
% dG = SeqFreeEnergy2(seq,loop,'FR',0.020 + 0.1 + 3.3*sqrt(0.003))*kT; % best fit for HP Seq 1 (I think?)
% dG = SeqFreeEnergy2(seq,loop,'FR',0.020 + 0.1 + 8.0*sqrt(0.003))*kT; % used in Zhi's paper, best fit for all those seqs
dG = SeqFreeEnergy2(seq,loop,'FR',0.020 + 0.0175 + 8.0*sqrt(0.005))*kT; % MM playing with parameters
dG = dG(1:end - length(loop)/2); 
dGDNA = meshgrid(cumsum([0 dG]),ones(1,length(xtot)));

% Trap
xb = F/ktrap;
Eb = 0.5*ktrap*xb.^2; %Trap energy

% ssDNA & dsDNA
dGds = [];
dGss = [];
for i = 1:lss+1
    for j = 1:length(xtot)
        dGss(j,i) = (2*(i-1)+ssbs)*hss*(F(j,i)*XWLCContour(F(j,i),Pss,Sss)-integral(@(f)XWLCContour(f,Pss,Sss),0,F(j,i))); %
        dGds(j,i) = lds*hds*(F(j,i)*XWLCContour(F(j,i),Pds,Sds)-integral(@(f)XWLCContour(f,Pds,Sds),0,F(j,i)));
    end
end

Gtot = Eb + dGds + dGss - dGDNA;
F_av = sum(F.*exp(-Gtot/kT),2)./sum(exp(-Gtot/kT),2); F_av = F_av(:)'; %Sum over all intermediate unwound states i
xtot = xtot-F_av/ktrap;
figure;
plot(xtot,F_av); 