% Script for conversion of DNA extension in nanometers to basepairs unwound for
% hairpin construct,has the option of plotting the reults at the end

[WLC_param] = WLC_parameters;
Hds = WLC_param.hds; % [nm/bp], interphosphate distance for ds DNA
Hss = WLC_param.hss; % nm/nt,  interphosphate distance for ss DNA
Pds = WLC_param.Pds; % nm persistence length for ds DNA
Pss = WLC_param.Pss; % nm persistence length for ss DNA
Sss = WLC_param.Sss; % pN elastic modulus for ss DNA
Sds = WLC_param.Sds; % pN elastic modulus for ds DNA
kT = WLC_param.kT;% Zhi's values (dsDNA)

hairpinwidth = 2.0;

lds = 3055; %length of ds handles
lss = 10; %length of ss loading site
lhp = 89; %length of hairpin
lloop = 4; %length of T loop in hairpin

ssclosed = lss;
ssopen = lss+(2*lhp)+lloop;

if param.direction == 'Y'
    xclosed = lds*Hds*XWLCContour(trapData.FmeanY,Pds,Sds) + ssclosed*Hss*XWLCContour(trapData.FmeanY,Pss,Sss) + hairpinwidth;
end

if param.direction == 'X'
    xclosed = lds*Hds*XWLCContour(trapData.FmeanX,Pds,Sds) + ssclosed*Hss*XWLCContour(trapData.FmeanX,Pss,Sss) + hairpinwidth;
    %xclosed = lds*Hds*XWLCContour(trapData.FmeanX,Pds,Sds) + ssclosed*Hss*XWLCContour(trapData.FavgX,Pss,Sss) + hairpinwidth;

end


if param.direction == 'Y';
    trapData.DNAext_Ytot = trapData.DNAext_Y + xclosed;
    trapData.nbp = trapData.DNAext_Y./(2*Hss*XWLCContour(trapData.FmeanY,Pss,Sss));
end

if param.direction == 'X';
    trapData.DNAext_Xtot = trapData.DNAext_X + xclosed;
    trapData.nbp = trapData.DNAext_X./(2*Hss*XWLCContour(trapData.FmeanX,Pss,Sss));
    %trapData.nbp = trapData.DNAext_X./(2*Hss*XWLCContour(trapData.FavgX,Pss,Sss));
end

% Plotting Options for Plotting Force and Number of Basepairs Unwound
if param.plot_bp == 1
        if param.direction == 'Y'
        figure
        ax1 = subplot(2,1,1);
        hold on
        plot(trapData.time, trapData.nbp)
        xlabel('time(s)')
        ylabel('Basepairs Unwound')
        legend([num2str(param.desired_trap_bw) ' Hz'])
        title({['Basepairs Unwound vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.FmeanY) ' pN']});
        ax2 = subplot(2,1,2);
        hold on
        plot(trapData.time, trapData.force_AY)
        hold on
        plot(trapData.time, trapData.force_BY)
        ylabel('Force (pN)')
        xlabel('time(s)')
        legend('AY','BY')
        title('Constant Force Data vs. Time')
        linkaxes([ax1, ax2], 'x')
        %savefig(['File ' num2str(param.datafnumber,'%03d') ' force and base pairs unwound updated']);
        fpath_save = [datadirectory '\processed data and figures\'];
        name_save = ['File ' num2str(param.datafnumber,'%03d') ' force and base pairs unwound.fig'];
        saveas(gca, fullfile(fpath_save, name_save));
        end
        
        if param.direction == 'X'
        figure
        ax1 = subplot(2,1,1);
        hold on
        plot(trapData.time, trapData.nbp)
        xlabel('time(s)')
        ylabel('Basepairs Unwound')
        legend([num2str(param.desired_trap_bw) ' Hz'])
        title({['Basepairs Unwound vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.FmeanX) ' pN']});
        ax2 = subplot(2,1,2);
        hold on
        plot(trapData.time, trapData.force_AX)
        hold on
        plot(trapData.time, trapData.force_BX)
        hold on
        plot(trapData.time, trapData.force_AY)
        hold on
        plot(trapData.time, trapData.force_BY)
        ylabel('Force (pN)')
        xlabel('time(s)')
        legend('AX','BX','AY','BY')
        title('Constant Force Data vs. Time')
        linkaxes([ax1, ax2], 'x')
       % savefig(['File ' num2str(param.datafnumber,'%03d') ' force and basepairs unwound updated']);
        fpath_save = [datadirectory '\processed data and figures\'];
        name_save = ['File ' num2str(param.datafnumber,'%03d') ' force and basepairs unwound .fig'];
        saveas(gca, fullfile(fpath_save, name_save));
        end
                
end        