global analysis
global rawDataPath       % always run this code before starting to process data
global analysisPath
global experimentPath     
global datadirectories
global laptop
global fbslash
global newLVcode
global computer
global calparams
global trap_computer
global beaddiameters

laptop = 1;
computer = 'Mac';
trap_computer = 0;


fbslash = '\';
newLVcode = 1;
set(0,'defaultfigureposition',[25  55  1050  659]') % Put figures in middle of laptop screen.
set(0,'defaultAxesFontName', 'Myriad Pro') % Change from Helvetica, since Adobe Illustrator can't recognize that font.
set(0,'defaultTextFontName', 'Myriad Pro')
set(0,'defaultTextFontSize', 22)
set(0,'defaultAxesFontSize', 20)

analysis = 'C:\Users\chemlalab\Documents\MATLAB\';

rawDataPath = 'E:\June 17 2021\';

analysisPath = 'E:\June 17 2021\';

datadirectories = 'E:\June 17 2021\';

experimentPath = 'C:\Users\chemlalab\Documents\MATLAB\';


% added to make Kevin's calibration program work

            %beadA beadB    fsamp   fxyhi  fsumhi   flo    avwin
calparams = [845  818  125000  62000   8000    600     200];  % usually do 62,000 for high and 600 for low
% calparams = [845  818  66000  62000   8000    600     200];% different sampling frequency on fleezers
beaddiameters = [845 818];  % usually 845 and 818
% cd(analysis)