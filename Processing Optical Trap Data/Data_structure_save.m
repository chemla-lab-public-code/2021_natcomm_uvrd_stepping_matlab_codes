% Short script to save calibration, time trace, offset, and force extension
% data as structure variables

save_path = [datadirectory '\processed data and figures\'];
cd(save_path)
save([param.Date '_' num2str(param.datafnumber,'%03d') '.trapData.mat'],'-struct','trapData');
save([param.Date '_' num2str(param.calfilenumber,'%03d') '.calData.mat'],'-struct','calData');
save([param.Date '_' num2str(param.rasterfilenumber,'%03d') '.offsetData.mat'],'-struct','offsetData');
save([param.Date '_' num2str(param.fextfnumber,'%03d') 'fextData.mat'],'-struct','fextData');

