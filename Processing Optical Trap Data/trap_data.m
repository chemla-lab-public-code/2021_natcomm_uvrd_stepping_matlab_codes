% This code processes trap data. 
% 02-04-2018
% DO NOT MODIFY!!   
% 
% Input variables:
%     Date: date at which the data was acquired, e.g. '170806'
%     calfnumber: number corresponding to the calibration file, e.g. 141
%     datafnumber: number corresponding to the trace file, e.g. 145
%     calData: data structure output from Calibrate.m
%     plot_option: gives option to plot all of the parameters at the end
% Output structure array called trapData contains:
%     trapData.TrapSep: trap separation in nm (used conversion factors - see below)
%     trapData.A_X: QPD data in volts for bead A in x
%     trapData.B_X: QPD data in volts for bead B in x
%     trapData.A_Y: QPD data in volts for bead A in y
%     trapData.B_Y: QPD data in volts for bead B in y
%     trapData.A_Xnm: QPD data in nanometers for bead A in x
%     trapData.B_Xnm: QPD data in nanometers for bead B in x
%     trapData.A_Ynm: QPD data in nanometers for bead A in y
%     trapData.B_Ynm: QPD data in nanometers for bead B in y
%     trapData.DNAext_X: DNA extension in nanometers in x
%     trapData.DNAext_Y: DNA extension in nanometers in y
%     trapData.force_AX: force in picoNewtons for bead A in x
%     trapData.force_BX: force in picoNewtons for bead B in x
%     trapData.force_AY: force in picoNewtons for bead A in y
%     trapData.force_BY: force in picoNewtons for bead B in y
%     trapData.time: time in seconds
% The variables stored in structure array called traceData are used to 
% measure trap separation. Note: To get trap separation on the old-trap, 
% we use Pythagoras's Theorem. The AOM on the fleezers is setup in X only.
% The variables in traceData are defined here:
%     traceData.trappos1 - moving trap position in X
%     traceData.trappos2 - moving trap position in Y
%     traceData.t1x: start position trap 1 in x
%     traceData.t1y: start position trap 1 in y
%     traceData.t2x: start position trap 2 in x
%     traceData.t2y: start position trap 2 in y
%     traceData.t1y - t2y: distance in y
%     traceData.t1x - t2x: distance in x
% Conversion factors from voltage or frequency to nm, as of 02-04-2018:
%     flconv = 123: conversion factor in nm/MHz from grid measurements for fleezers 
%     otconvX = 891.5: % conversion factor in nm/V from grid measurements for old-trap in X 
%     otconvY = 505.7; % (nm/V) conversion factor in nm/V from grid measurements for old-trap in Y


function trapData  = trap_data(Date,calfnumber,datafnumber,instrument, datadirectory, plot_option)
global fbslash
global rawDataPath
global datadirectories

flconv = 123; 
otconvX = 883.59; 
otconvY = 508.93;

if nargin == 0
    param.instrument = 0; % 0 - fl, 1 - ot
    Date = '180824'; % this may be changed
    calfnumber = 104; % this may be changed
    datafnumber = 109; % this may be changed
    plot_option = 1;
    rootTraceFile = [Date '_' num2str(datafnumber,'%03d') '.dat'];
    rootCalFile = [Date '_' num2str(calfnumber,'%03d') '.dat'];
        if param.instrument == 0
            startpath = [datadirectories Date fbslash];
        elseif param.instrument == 1
            startpath = [rawDataPath Date fbslash];
        end
else
    param.instrument = instrument;
    rootTraceFile = [Date '_' num2str(datafnumber,'%03d') '.dat'];
    rootCalFile = [Date '_' num2str(calfnumber,'%03d') '.dat'];
    startpath = datadirectory;
end


% calparams = [900    880     66000  21000    8000    1000     100]; %62500
% calparams = [845   818    125000  62000    8000    600     200]; % 850 900 125000 10000 8000 900 100 usually
 calparams = [845   818    66000  62000    8000    600     200]; % different bandwidth on fleezers
calData = calibrate(Date,rootCalFile,calparams,0,0);
traceData = ReadJointFile(startpath,rootTraceFile);  

%% V or MHz CONVERSION TO nm
if param.instrument == 0
        trapData.TrapSep_X = (traceData.trappos2 - traceData.trappos1)*flconv; 
        trapData.TrapSep_Y = 0;
        trapData.TrapSep = sqrt((trapData.TrapSep_X).^2 + (trapData.TrapSep_Y).^2);
elseif param.instrument == 1
        trapData.TrapSep_X = (traceData.trappos1 - traceData.t2x)*otconvX;
        trapData.TrapSep_Y = (traceData.trappos2 - traceData.t2y)*otconvY;
        trapData.TrapSep = sqrt(((traceData.trappos1 - traceData.t2x)*otconvX).^2 + ((traceData.trappos2 - traceData.t2y)*otconvY).^2);
end

%% QPD OFFSET SUBTRACTION
trapData.A_X = traceData.A_X-calData.AXoffset; % both are in volts
trapData.B_X = traceData.B_X-calData.BXoffset; % both are in volts
trapData.A_Y = traceData.A_Y-calData.AYoffset; % both are in volts
trapData.B_Y = traceData.B_Y-calData.BYoffset; % both are in volts

%% QPD DATA IN NANOMETERS
trapData.A_Xnm = (trapData.A_X)*calData.alphaAX; % convert QPD data from volts to nm  %multiply by -1 later
trapData.B_Xnm = (-trapData.B_X)*calData.alphaBX; % convert QPD data from volts to nm %multiply by -1 later
trapData.A_Ynm = (-trapData.A_Y)*calData.alphaAY; % convert QPD data from volts to nm 
trapData.B_Ynm = (trapData.B_Y)*calData.alphaBY;  % convert QPD data from volts to nm

%% DNA EXTENSION IN NANOMETERS
trapData.DNAext_X = trapData.TrapSep - trapData.A_Xnm - trapData.B_Xnm - calData.beadA/2 - calData.beadB/2;
trapData.DNAext_Y = trapData.TrapSep - trapData.A_Ynm - trapData.B_Ynm - calData.beadA/2 - calData.beadB/2;

%% FORCE (pN)
trapData.force_AX = trapData.A_Xnm * calData.kappaAX; % multiply QPD data by trap stiffness
trapData.force_BX = trapData.B_Xnm * calData.kappaBX; % multiply QPD data by trap stiffness
trapData.force_AY = trapData.A_Ynm * calData.kappaAY; % multiply QPD data by trap stiffness
trapData.force_BY = trapData.B_Ynm * calData.kappaBY; % multiply QPD data by trap stiffness
trapData.time = traceData.time;
trapData.sampperiod = traceData.sampperiod;

% PLOT
if plot_option
    figure	
    subplot(3,1,1)
    plot(trapData.time, trapData.TrapSep)
    ylabel('Trap Separation (nm)')
    legend('Trap Separation')
    title({['Trap Position vs. Time (' Date '\_' num2str(datafnumber,'%03d') ')']});
    hold on
    subplot(3,1,2)
    plot(trapData.time, trapData.DNAext_X)
    hold on
    plot(trapData.time, trapData.DNAext_Y)
    ylabel('DNA extension (nm)')
    legend('DNA extension in x', 'DNA extension in y')
    subplot(3,1,3)
    plot(trapData.time, trapData.force_AX)
    hold on
    plot(trapData.time, trapData.force_BX)
    hold on
    plot(trapData.time, trapData.force_AY)
    hold on
    plot(trapData.time, trapData.force_BY)
    ylabel('Force (pN)')
    legend('force bead A in x', 'force bead B in x', 'force bead A in y', 'force bead B in y')
end 
end
