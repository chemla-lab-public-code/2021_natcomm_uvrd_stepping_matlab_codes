clear calData          % to use this code, first enter all desired parameters and
clear fextData         % data saving location below, and enter/select the desired
clear offsetData       % information for "datasets".  Then, uncomment the datasets
clear trapData         % vector for the desired trace, and run the script.
                       % The code extracts the necessary data from the
                       % calibration files, offset, and time trace files to 
                       % calculate DNA extension in nm versus time, then
                       % plots the results.
                

global calparams
% SPC host code
datasets = {...
%{'180701' 261 262 264 265 [0 205] [0 30] '1'}...%    July 1, 2018
%{'180702' 043 044 051 052 [0 415] [0 15] '5'}...% in x
%{'180702' 043 044 051 053 [0 237] [0 25] '5'}...% back to y
%{'180702' 067 068 069 070 [0 185] [0 30] '7'}...% 
%{'180702' 082 083 084 087 [0 132] [0 30] '8'}...% 
%{'180702' 082 083 084 090 [0 245] [0 40] '8'}...% 
%{'180702' 082 083 084 091 [0 193] [0 28] '8'}...% 

%{'180704' 014 015 017 018 [0 140] [0 30] '2'}...%
%{'180704' 014 015 017 019 [0 50] [0 30] '2'}...%      July 4 2018
%{'180704' 062 063 065 068 [0 90] [0 20] '5'}...%
%{'180704' 083 084 087 088 [0 95] [0 40] '6'}...%
%{'180704' 106 107 109 110 [0 90] [0 30] '7'}...%
%{'180704' 106 107 109 111 [0 127] [0 20] '7'}...%
%{'180704' 147 148 149 150 [0 25] [0 2] '9'}...%
%{'180704' 147 148 149 151 [0 161] [0 25] '9'}...%
%{'180704' 147 148 149 152 [0 129] [0 30] '9'}...%
    
%{'180706' 166 167 168 169 [0 73] [0 30] '1'}...%
%{'180706' 166 167 168 000 [0 223] [0 35] '1'}...%  July 5-6 2018
%{'180706' 166 167 168 001 [0 93] [0 30] '1'}...%
%{'180706' 166 167 168 003 [0 200] [0 30] '1'}...%
%{'180706' 166 167 168 004 [0 143] [0 30] '1'}...%
%{'180706' 166 167 168 005 [0 63] [0 25] '1'}...%
%{'180706' 166 167 168 006 [0 107] [0 22] '1'}...%
%{'180706' 166 167 168 007 [6 111] [6 40] '1'}...%
%{'180706' 166 167 168 008 [0 175] [0 30] '2'}...%
%{'180706' 166 167 168 009 [0 232] [0 25] '2'}...%
%{'180706' 166 167 168 010 [0 69] [0 20] '2'}...%
%{'180706' 166 167 168 011 [0 170] [0 30] '2'}...%
%{'180706' 031 032 036 037 [0 121] [0 30] '3'}...%
%{'180706' 031 032 036 038 [0 115] [0 40] '3'}...%
%{'180706' 031 032 036 039 [0 68] [0 30] '3'}...%
%{'180706' 061 62 064 066 [0 120] [0 30] '4'}...%
%{'180706' 085 86 088 089 [16 130] [16 31] '5'}...% 
%{'180706' 102 103 104 105 [0 164] [0 40] '6'}...%
%{'180706' 102 103 104 106 [0 221] [0 30] '6'}...%
%{'180706' 102 103 104 107 [0 219] [0 24] '6'}...%
%{'180706' 102 103 104 108 [0 319] [0 25] '7'}...%
%{'180706' 102 103 104 109 [0 214] [0 25] '7'}...%
%{'180706' 102 103 104 110 [0 143] [0 26] '7'}...%
%{'180706' 102 103 104 111 [0 311] [0 30] '8'}...%
%{'180706' 102 103 104 113 [0 250] [0 34] '8'}...%

%{'180710' 005 006 008 010 [0 120] [0 40] '1'}...%
%{'180710' 011 012 015 018 [0 116] [0 20] '2'}...%
%{'180710' 106 107 108 109 [0 70] [0 30] '5'}...%  July 10 2018
%{'180710' 112 113 117 118 [0 132] [0 40] '6'}...%

%{'180919' 107 108 113 114 [0 446] [0 30] '1'}...%  

%{'180920' 026 027 030 031 [5 310] [5 40] '1'}...%    
%{'180920' 026 027 030 032 [0 282] [0 30] '1'}...%    
%{'180920' 106 107 110 111 [0 650] [0 40] '2'}...%    
%{'180921' 032 033 035 036 [0 324] [0 40] '4'}...%    
%{'180921' 110 111 112 113 [0 390] [0 35] '5'}...%   
%{'180921' 110 111 114 115 [0 580] [0 30] '6'}...%    
%{'180921' 289 290 292 293 [0 145] [0 20] '3'}...%    
%{'180921' 346 347 349 350 [10 292] [10 40] '4'}...%  
%{'180921' 367 368 372 373 [0 307] [0 25] '5'}...%    
%{'180921' 390 391 392 393 [0 159] [0 30] '6'}...%    

%{'180924' 029 030 032 033 [0 124] [0 40] '1'}...%    
%{'180924' 092 093 095 096 [0 130] [0 40] '2'}...%    
%{'180924' 122 123 125 126 [0 63] [0 20] '3'}...%    
%{'180924' 122 123 125 127 [0 68] [0 20] '3'}...%     
%{'180924' 122 123 125 128 [0 101] [0 30] '3'}...%    
%{'180924' 199 200 203 204 [0 194] [0 30] '4'}...%    
%{'180924' 199 200 203 205 [0 139] [0 25] '5'}...%    
%{'180924' 267 268 270 271 [0 80] [0 25] '6'} ...%    
%{'180925' 046 047 048 049 [0 100] [0 20] '8'}...%   
%{'180925' 046 047 048 051 [0 250] [0 30] '8'}...%    

                                                      
                         
%{'190131' 266 267 271 272 [0 250] [0 30] '1'}...%                
%{'190131' 266 267 271 273 [0 204] [0 40] '1'}...%  
% Feb 1 2019 

%{'190201' 085 086 089 090 [0 150] [0 30] '1'}...%
%{'190201' 085 086 089 091 [0 15] [0 5] '1'}...% 
%{'190201' 085 086 089 092 [0 32] [0 10] '1'}...% 
%{'190201' 093 094 098 099 [0 102] [0 30] '2'}...%                                                                                             
%{'190201' 093 094 098 100 [0 196] [0 40] '2'}...% 
%{'190201' 107 108 111 112 [0 209] [0 30] '3'}...% 
%{'190201' 107 108 111 113 [0 218] [0 25] '3'}...% 
%{'190201' 107 108 111 114 [0 152] [0 30] '3'}...%
%{'190201' 107 108 111 115 [0 254] [0 25] '3'}...% 
%{'190201' 107 108 111 116 [0 401] [0 30] '3'}...% 
%{'190201' 107 108 111 117 [0 177] [0 25] '3'}...% 
%{'190201' 135 136 138 139 [0 242] [0 32] '4'}...% 
%{'190201' 135 136 138 140 [0 54] [0 25] '4'}...% 
%{'190201' 135 136 138 141 [0 116] [0 13] '4'}...% 
%{'190201' 143 144 150 151 [0 92] [0 30] '5'}...% 
%{'190201' 155 156 157 158 [0 201] [0 30] '6'}...% 
%{'190201' 155 156 157 159 [0 123] [0 30] '6'}...% 
%{'190201' 189 190 197 198 [0 536] [0 30] '7'}...% 
%{'190201' 236 237 239 240 [4 300] [4 40] '8'}...% 
%{'190202' 019 020 022 023 [0 77] [0 30] '9'}...%  

% Feb 4 2019 

%{'190204' 029 030 031 032 [0 198] [0 40] '1'}...% 
%{'190204' 029 030 031 033 [0 175] [0 16] '1'}...% 
%{'190204' 029 030 031 034 [0 77] [0 30] '1'}...% 
%{'190204' 029 030 031 035 [0 227] [0 30] '1'}...% 
%{'190204' 059 060 066 067 [8 186] [8 39] '2'}...% 
%{'190204' 083 084 085 086 [0 160] [0 30] '3'}...% 
%{'190204' 087 088 091 092 [0 462] [0 40] '4'}...% 
%{'190204' 096 097 100 101 [0 202] [0 20] '5'}...% 
%{'190204' 096 097 103 104 [0 402] [0 35] '6'}...% 
%{'190204' 141 142 148 149 [0 35] [0 15] '7'}...% 
%{'190204' 141 142 148 150 [0 76] [0 25] '7'}...% 
%{'190204' 141 142 148 151 [0 49] [0 20] '7'}...% 
%{'190204' 141 142 148 152 [0 30] [0 15] '7'}...% 
%{'190204' 141 142 148 153 [0 70.5] [0 30] '7'}...% 
%{'190204' 178 179 180 181 [0 381] [0 30] '8'}...% 
%{'190204' 178 179 180 182 [0 233] [0 40] '8'}...% 
%{'190204' 178 179 180 183 [0 214] [0 30] '8'}...% 
%{'190204' 178 179 180 184 [0 377] [0 35] '8'}...% 
%{'190204' 178 179 180 185 [0 171] [0 40] '8'}...% 
%{'190204' 189 190 193 194 [0 507] [0 35] '9'}...% 
%{'190204' 189 190 193 195 [0 214] [0 28] '9'}...% 
%{'190205' 020 021 024 025 [0 166] [0 20] '10'}...% 
%{'190205' 026 027 030 031 [0 272] [0 35] '11'}...% 
%{'190205' 026 027 030 032 [0 388] [0 32] '11'}...% 
%{'190205' 061 062 065 066 [0 110] [0 30] '12'}...% 
%{'190205' 112 113 118 119 [0 165] [0 30] '13'}...% 



% February 13 2019 

%{'190213' 089 090 096 097 [0 149] [0 30] '1'}...% 
%{'190213' 089 090 096 098 [0 342] [0 35] '1'}...% 
%{'190213' 089 090 099 100 [0 12] [0 10] '2'}...% 
%{'190213' 089 090 099 101 [0 98] [0 30] '2'}...% 
%{'190213' 158 159 162 163 [0 541] [0 35] '3'}...% 
%{'190213' 187 188 190 191 [0 268] [0 25] '4'}...% 
%{'190213' 187 188 192 193 [0 185] [0 30] '5'}...% 
%{'190213' 187 188 192 194 [0 379] [0 31] '5'}...% 
%{'190213' 187 188 195 196 [0 411] [0 35] '6'}...% 

% February 14 2019 

%{'190214' 086 087 088 089 [0 275] [0 40] '1'}...% 
%{'190214' 098 099 100 101 [0 212] [0 35] '2'}...% 
%{'190214' 098 099 102 103 [0 108] [0 30] '3'}...% 
%{'190214' 098 099 102 104 [0 142] [0 25] '3'}...% 
%{'190214' 216 217 218 219 [0 172] [0 30] '4'}...% 
%{'190214' 216 217 218 220 [0 12] [8 12] '4'}...% 
%{'190214' 216 217 218 221 [0 276] [0 30] '4'}...% 
%{'190214' 216 217 225 226 [0 265] [0 30] '5'}...% 
%{'190214' 227 228 231 232 [0 296] [0 40] '6'}...% 
%{'190215' 022 023 027 028 [0 344] [0 34] '7'}...% 

% May 7 2019 

%{'190508' 017 018 021 022 [0 281] [0 45] '1'}...%
%{'190508' 017 018 021 023 [0 464] [0 44] '1'}...%
%{'190508' 017 018 025 026 [0 32.67] [0 4] '2'}...%
%{'190508' 017 018 025 026 [0 32.67] [0 4] '2'}...%
%{'190508' 060 061 062 063 [0 112] [0 20] '3'}...%
%{'190508' 060 061 062 064 [0 271] [0 27] '3'}...%
%{'190508' 060 061 062 065 [0 146] [0 15] '3'}...%
%{'190508' 068 069 072 073 [1.4 49] [1.4 15] '4'}...%
%{'190508' 076 077 081 082 [0 39] [0 10] '5'}...%                   

                                                     % May 31 2019 data
%{'190531' 033 034 038 039 [0 182] [0 30] '2'}...%                                                     
%{'190531' 092 093 101 102 [0 318] [0 25] '4'}...%
%{'190531' 092 093 101 103 [0 226] [0 34] '4'}...%
%{'190531' 162 163 168 169 [0 74.5] [0 30] '5'}...%
%{'190531' 189 190 196 197 [0 258] [0 40] '6'}...%
%{'190601' 109 110 114 115 [0 636] [0 35] '10'}...%
%{'190601' 188 189 195 196 [0 43] [0 10] '14'}...%


                                                     % August 29 2019 data
%{'190829' 036 037 042 043 [0 20] [2 10] '1'}...% 
%{'190829' 059 060 061 065 [0 70] [0 20] '2'}...%
%{'190829' 071 072 073 079 [0 22.5] [0 10] '3'}...%
%{'190829' 083 084 085 088 [0 65] [0 20] '4'}...%
%{'190829' 114 115 116 119 [0 22] [2 5] '5'}...%
%{'190829' 131 132 133 137 [0 84] [0 30] '6'}...%
%{'190829' 141 142 143 145 [0 76] [0 20] '7'}...%

%{'190826' 100 101 102 109 [0 68] [0 20] '1'}...%

%{'190820' 137 138 139 143 [0 23.5] [0 10] '1'}...%
%{'190820' 137 138 139 144 [0 36] [0 10] '1'}...%
%{'190812' 044 045 046 049 [0 87] [0 30] '1'}...%
%{'190812' 065 066 067 072 [0 9.2] [0 3] '2'}...%
%{'190812' 065 066 067 073 [0 63] [0 10] '2'}...%
%{'190812' 093 094 095 102 [0 99] [0 25] '3'}...%
%{'190812' 120 121 122 129 [0 110] [0 25] '4'}...%
%{'190810' 122 123 124 135 [0 78] [0 20] '1'}...%
%{'190807' 146 147 148 155 [0 111] [0 19] '1'}...%

                                                     % June 2019 examples
%{'190619' 137 138 139 144 [0 20] [0 10] '1'}...%
%{'190620' 028 029 030 037 [0 50] [0 20] '2'}...%
%{'190622' 093 094 095 102 [0 114] [0 20] '1'}...%
%{'190617' 127 128 129 131 [0 120] [0 40] '1'}...%

%{'190912' 068 069 070 071 [15.2 605] [15.2 60] '1'}...%
%{'190912' 125 126 132 134 [0 40] [0 15] '2'}...%
%{'190912' 125 126 132 135 [0 45] [0 10] '2'}...%
%{'190912' 125 126 132 136 [0 140] [0 40] '2'}...%
%{'190912' 143 144 150 151 [0 234.4] [0 38] '3'}...%
%{'190916' 029 030 031 033 [0 140] [0 22] '1'}...%
%{'190916' 099 100 101 104 [0 216.9] [0 22] '2'}...%
%{'190917' 019 020 021 027 [0 256] [0 40] '3'}...%
%{'190917' 266 267 268 269 [0 347] [0 34] '1'}...% 
%{'190919' 370 371 372 374 [0 406] [0 39] '1'}...%
%{'190919' 380 381 382 383 [0 382] [0 25] '2'}...%
%{'190920' 101 102 103 104 [0 534] [0 30] '3'}...%
%{'190920' 101 102 108 110 [0 455] [0 35] '4'}...%
%{'190921' 008 009 011 012 [0 926] [0 35] '1'}...%
%{'190921' 008 009 014 015 [0 231] [0 30] '1'}...%
%{'190921' 128 129 135 137 [0 419] [0 30] '2'}...%
%{'190920' 013 014 019 020 [0 176] [0 30] '5'}...%
%{'190920' 013 014 019 022 [0 1186] [0 34] '5'}...%
%{'191002' 083 084 085 087 [27 426] [27 45] '1'}...%
%{'191003' 042 043 044 049 [0 105] [0 20] '2'}...%
%{'191024' 075 076 078 082 [0 343] [0 30] '1'}...%
%{'191024' 075 076 087 088 [0 333] [0 20] '1'}...%
%{'191106' 188 189 190 195 [14 592] [14 25] '1'}...%
%{'191106' 188 189 190 198 [0 239.5] [0 24] '1'}...%
%{'191107' 049 050 051 053 [1 530] [1 30] '1'}...%
%{'191107' 049 050 051 053 [1 530] [1 30] '1'}...%
%{'191107' 058 059 060 063 [18 607.8] [18 45] '2'}...%
%{'191109' 122 123 124 126 [0 576] [0 28] '1'}...%
%{'191111' 233 234 248 241 [0 353] [0 35] '1'}...%
%{'191111' 233 234 248 243 [3 339] [3 33] '1'}...%
%{'191115' 122 123 136 138 [10 735] [10 35] '1'}...%
%{'191115' 168 169 170 171 [5 219] [5 30] '2'}...%
%{'191117' 053 054 055 059 [0 739] [0 30] '1'}...%
%{'191118' 131 132 133 137 [11.34 155] [11.34 25] '2'}...%
%{'191118' 292 293 294 295 [0 616] [0 25] '1'}...%
%{'191119' 121 122 125 126 [0 1079] [0 35] '2'}...%
%{'191119' 121 122 125 128 [0 492] [0 30] '2'}...%
%{'191124' 479 480 484 485 [0 911] [0 25] '1'}...%
%{'191121' 030 031 035 036 [0 225] [0 25] '1'}...%
%{'191121' 030 031 054 055 [0 302] [0 21] '1'}...%
%{'191124' 247 248 250 251 [0 1038] [0 20] '1'}...%
%{'191124' 302 303 306 307 [0 53] [0 20] '2'}...%
%{'191124' 447 448 449 450 [0 304] [0 35] '2'}...%
%{'191107' 058 059 060 063 [18 607.8] [18 45] '2v2'}...% 
%{'191107' 058 059 060 063 [18 607.8] [18 45] '2v3'}...% 

%{'210615' 029 030 031 032 [0 102] [0 10] '1'}...%
%{'210615' 029 030 031 033 [0 284] [0 50] '1'}...%
%{'210615' 060 061 062 063 [0 300] [0 30] '2'}...%
%{'210615' 080 081 083 084 [0 314] [0 30] '3'}...%
%{'210615' 085 086 087 088 [0 113] [0 20] '4'}...%
%{'210615' 096 097 100 101 [0 147] [45 75] '5'}...%
%{'210615' 096 097 100 102 [0 141] [0 40] '5'}...%
%{'210615' 096 097 100 103 [0 294] [0 50] '5'}...%
%{'210615' 113 114 117 118 [0 81] [0 30] '6'}...%
%{'210615' 149 150 153 154 [0 341] [0 40] '7'}...%
%{'210615' 167 168 173 174 [0 86] [0 23] '8'}...%
%{'210616' 022 023 025 026 [0 228] [0 50] '1'}...%
%{'210616' 054 055 056 057 [0 33] [0 20] '2'}...%
%{'210616' 095 096 097 098 [0 388] [0 40] '3'}...%
%{'210616' 140 141 142 143 [0 489] [0 45] '4'}...%
%{'210616' 195 196 197 198 [0 27] [0 10] '5'}...%
%{'210617' 009 010 014 015 [0 389] [0 35] '1'}...%
%{'210617' 049 050 053 054 [0 447] [0 30] '2'}...%
%{'210617' 055 056 059 060 [0 60] [0 20] '3'}...%
%{'210617' 065 066 069 070 [0 198] [0 20] '4'}...%
%{'210617' 079 080 084 085 [0 132] [0 20] '5'}...%
%{'210617' 079 080 084 086 [0 146] [0 40] '5'}...%
%{'210617' 175 176 177 178 [0 638] [0 50] '6'}...%
%{'210617' 191 192 195 196 [0 2.4] [0 1] '7'}...%
%{'210617' 191 192 198 199 [0 185] [20 60] '7'}...%
};


construct = 'Hairpin-RH-dT-10';

param.instrument = 1; % 0 - fleezers; 1 - ot;
param.direction = 'X'; % 'X' or 'Y'

param.offset_subtraction = 1; % subtract the force offset 
param.baseline_subtraction = 1; % subtract the baseline
param.plot_trace = 1; % plot extension
param.plot_bp = 1; % plot basepairs unwound
param.plot_fext = 0; % plot force vs extension curve
param.plotCal = 0; % plot calibration
param.plot_other = 1; % plot total DNA extension, average force, etc., in separate figures
param.desired_trap_bw = 100; % downsampling frequency in Hz, 100 on OT,
%267 on fleezers


param.overlay_fext = 0;

param.model_hairpin_unwind = 0;

param.Date = datasets{1}{1}; % date
param.calfilenumber = datasets{1}{2}; % calibration file number
param.rasterfilenumber = datasets{1}{3}; % offset file number
param.fextfnumber = datasets{1}{4}; % force extension curve file number
param.datafnumber = datasets{1}{5}; % trace file number
param.plotinterval = datasets{1}{6}; % (s)
param.baselineinterval = datasets{1}{7}; % (s)
param.number = datasets{1}{8}; % number specifying folder with specific time trace

% Title Options
param.system = 'wtUvrD';
param.DNA = 'HP 1 10 dT';
param.experiment = 'single turnover';
param.proteinconc = '10 nM protein';
param.ATPconc = '1 uM';

% call the data directory
if param.instrument == 1 
        datadirectory = ['E:\June 17 2021\time traces\trace ' param.number '\' param.Date '\'];
elseif param.instrument == 0
        datadirectory = ['E:\June 17 2021\time traces\trace ' param.number '\' param.Date '\'];
end

% get the names of the files in the data directory
traceRootFile = [param.Date '_' num2str(param.datafnumber,'%03d') '.dat']; datafilename = ['data_' traceRootFile(1:(end-4))];
calRootFile = [param.Date '_' num2str(param.calfilenumber,'%03d') '.dat']; calfilename = ['cal' calRootFile(1:(end-4))];
offsetRootFile = [param.Date '_' num2str(param.rasterfilenumber,'%03d') '.dat']; offsetfilename = ['offset' offsetRootFile(1:(end-4))];

% get calibration

calData = calibrate(param.Date,calRootFile,calparams,0,0);

% get the data without force offset subtraction
trapData  = trap_data(param.Date,param.calfilenumber,param.datafnumber,param.instrument,datadirectory,0);
offsetData  = trap_data(param.Date,param.calfilenumber,param.rasterfilenumber,param.instrument,datadirectory,0);
fextData  = trap_data(param.Date,param.calfilenumber,param.fextfnumber,param.instrument,datadirectory,0); 

% update the data with force offset subtraction
if param.offset_subtraction == 1;
    trapData = force_offset_subtraction(offsetData,trapData, calData, param);   % originally was trapDatafinal, changed on 7/23/18
end


% plot the force extension curve
fextData = force_offset_subtraction(offsetData,fextData,calData, param); % originally was fextDatafinal, changed on 7/23/18
if param.plot_fext == 1;
   plot_force_extension(fextData, construct, param)
 savefig(['File ' num2str(param.fextfnumber,'%03d') ' force extension curve new code.fig'])
%     close
end

%% plotting interval
    [~, m1] = min(abs(trapData.time - param.plotinterval(1)));
    [~, m2] = min(abs(trapData.time - param.plotinterval(2)));
    crop = m1:m2;    
    %% BASELINE
    if param.baseline_subtraction == 1
    [~, b1] = min(abs(trapData.time - param.baselineinterval(1)));
    [~, b2] = min(abs(trapData.time - param.baselineinterval(2)));
    baseline = b1:b2;
    trapData.DNAext_X = trapData.DNAext_X - mean(trapData.DNAext_X(baseline));
    trapData.DNAext_Y = trapData.DNAext_Y - mean(trapData.DNAext_Y(baseline));
    end
    %% crop the data using plotting interval
    trapData.time = trapData.time(crop);
    trapData.time = trapData.time - param.plotinterval(1);
    trapData.DNAext_Y =  trapData.DNAext_Y(crop);
    trapData.DNAext_X =  trapData.DNAext_X(crop);
    trapData.TrapSep = trapData.TrapSep(crop);
    trapData.force_AX = trapData.force_AX(crop);
    trapData.force_BX = trapData.force_BX(crop);
    trapData.force_AY = trapData.force_AY(crop);
    trapData.force_BY = trapData.force_BY(crop);
%     trapData.averageAX = mean([trapData.force_AX]);
%     trapData.averageBX = mean([trapData.force_BX]);
%     trapData.averageAY = mean([trapData.force_AY]);
%     trapData.averageBY = mean([trapData.force_BY]);
    trapData.averageAX = nanmean([trapData.force_AX]);  % added on 6/23/2021 by SPC
    trapData.averageBX = nanmean([trapData.force_BX]);
    trapData.averageAY = nanmean([trapData.force_AY]);
    trapData.averageBY = nanmean([trapData.force_BY]);

    %% DOWNSAMPLE
    bw = 1/(trapData.time(2)-trapData.time(1)); % (ms)
    TrapAveFactor = round((1/trapData.sampperiod)/param.desired_trap_bw);
    trapData.time = downsample(trapData.time,TrapAveFactor);
    trapData.DNAext_X = downsample(trapData.DNAext_X,TrapAveFactor);
    trapData.DNAext_Y = downsample(trapData.DNAext_Y,TrapAveFactor);
    trapData.force_AX = downsample(trapData.force_AX,TrapAveFactor);
    trapData.force_BX = downsample(trapData.force_BX,TrapAveFactor);
    trapData.force_AY = downsample(trapData.force_AY,TrapAveFactor);
    trapData.force_BY = downsample(trapData.force_BY,TrapAveFactor);
    trapData.TrapSep = downsample(trapData.TrapSep,TrapAveFactor);
    
    %% Get the force averages
%     trapData.meanforce_AX = mean(trapData.force_AX);
%     trapData.meanforce_BX = mean(trapData.force_BX);
%     trapData.meanforce_AY = mean(trapData.force_AY);
%     trapData.meanforce_BY = mean(trapData.force_BY);

    trapData.meanforce_AX = nanmean(trapData.force_AX);
    trapData.meanforce_BX = nanmean(trapData.force_BX);
    trapData.meanforce_AY = nanmean(trapData.force_AY);
    trapData.meanforce_BY = nanmean(trapData.force_BY);

    
    trapData.FavgX = (trapData.force_AX + trapData.force_BX)/2;

    trapData.FavgY = (trapData.force_AY + trapData.force_BY)/2;

%     trapData.FmeanX = mean(trapData.FavgX);
%     trapData.FmeanY = mean(trapData.FavgY);

    trapData.FmeanX = nanmean(trapData.FavgX);
    trapData.FmeanY = nanmean(trapData.FavgY);
    
    
    % Plotting Options for Plotting Force and DNA extension in Nanometers
if param.plot_trace == 1
        if param.direction == 'Y'
        figure
        ax1 = subplot(2,1,1);
        hold on
        plot(trapData.time, trapData.DNAext_Y)
        xlabel('time(s)')
        ylabel('DNA extension (nm)')
        legend([num2str(param.desired_trap_bw) ' Hz'])
        title({['Trap Position vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.FmeanY) ' pN']});
        ax2 = subplot(2,1,2);
        hold on
        plot(trapData.time, trapData.force_AY)
        hold on
        plot(trapData.time, trapData.force_BY)
        ylabel('Force (pN)')
        xlabel('time(s)')
        legend('AY','BY')
        title('Constant Force Data vs. Time')
        linkaxes([ax1, ax2], 'x')
        fpath_save = [datadirectory '\processed data and figures\'];
        name_save = ['File ' num2str(param.datafnumber,'%03d') ' force and extension.fig'];
        saveas(gca, fullfile(fpath_save, name_save));
       % savefig(['File ' num2str(param.datafnumber,'%03d') ' force and extension updated']);
        end
        
        if param.direction == 'X'
        figure
        ax1 = subplot(2,1,1);
        hold on
        plot(trapData.time, trapData.DNAext_X)
        xlabel('time(s)')
        ylabel('DNA extension (nm)')
        legend([num2str(param.desired_trap_bw) ' Hz'])
        title({['Trap Position vs. Time (' param.Date '\_' num2str(param.datafnumber,'%03d') ')'] [ param.proteinconc ' ' param.system 'with ' param.DNA ', ' param.ATPconc ' ATP, ' num2str(trapData.FmeanX) ' pN']});
        ax2 = subplot(2,1,2);
        hold on
        plot(trapData.time, trapData.force_AX)
        hold on
        plot(trapData.time, trapData.force_BX)
        hold on
        plot(trapData.time, trapData.force_AY)
        hold on
        plot(trapData.time, trapData.force_BY)
        ylabel('Force (pN)')
        xlabel('time(s)')
        legend('AX','BX','AY','BY')
        title('Constant Force Data vs. Time')
        linkaxes([ax1, ax2], 'x')
        fpath_save = [datadirectory '\processed data and figures\'];
        name_save = ['File ' num2str(param.datafnumber,'%03d') ' force and extension.fig'];
        saveas(gca, fullfile(fpath_save, name_save));
      % savefig(['File ' num2str(param.datafnumber,'%03d') ' force and extension updated']);
        end               
end        

