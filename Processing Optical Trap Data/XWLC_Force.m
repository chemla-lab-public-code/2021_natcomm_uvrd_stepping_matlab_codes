function [Force] = XWLC_Force(x,P)
% short function to calculate force as a function of extension using XWLC
% Force is in pN
kb = 1.38e-23; % Boltzmann constant in J/K
T = 298; % Temperature in Kelvin

F_raw = x - (0.8*(x.^2.15)) + (0.25./((1 - x).^2)) - 0.25;

Force = ((F_raw*kb*T)/(P*(1.0e-9)))*(1.0e12);


end

