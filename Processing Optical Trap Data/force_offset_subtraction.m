function trapData = force_offset_subtraction(offsetData, trapData, calData, param)
% code subtracts offset from force data in time traces and force extension
% curves
% Sort offset
if param.direction == 'X';
    [s_extOffset, sid] = sort(offsetData.TrapSep);
    s_AOffset = offsetData.A_X(sid);   
    s_BOffset = offsetData.B_X(sid);  
elseif param.direction == 'Y';
    [s_extOffset, sid] = sort(offsetData.TrapSep);
    s_AOffset = offsetData.A_Y(sid);   
    s_BOffset = offsetData.B_Y(sid); 
end

% Smooth offset
Nfilt = round(1 / (trapData.sampperiod * 100)); % Divide by 10 to avoid oversmoothing
ss_extOffset = filter(ones(1,Nfilt), Nfilt, s_extOffset); 
ss_AOffset = filter(ones(1,Nfilt), Nfilt, s_AOffset); 
ss_BOffset = filter(ones(1,Nfilt), Nfilt, s_BOffset);

% Filter offset
sss_extOffset = ss_extOffset(Nfilt:end); 
sss_AOffset = ss_AOffset(Nfilt:end); 
sss_BOffset = ss_BOffset(Nfilt:end); 

[ssss_extOffset, index] = unique(sss_extOffset);
sss_AOffset = sss_AOffset(index);
sss_BOffset = sss_BOffset(index);

% Interpolate offset values corresponding to data values
ssi_AOffset = interp1(ssss_extOffset, sss_AOffset, trapData.TrapSep);
ssi_BOffset = interp1(ssss_extOffset, sss_BOffset, trapData.TrapSep);

% Replace the raw values with force offset subtracted values
if param.direction == 'X';
    trapData.A_X = trapData.A_X - ssi_AOffset; 
    trapData.B_X = trapData.B_X - ssi_BOffset;
elseif param.direction == 'Y';
    trapData.A_Y = trapData.A_Y - ssi_AOffset; 
    trapData.B_Y = trapData.B_Y - ssi_BOffset;
end

%% UPDATE QPD DATA IN NANOMETERS
trapData.A_Xnm = (trapData.A_X)*calData.alphaAX; % convert QPD data from volts to nm
trapData.B_Xnm = (-trapData.B_X)*calData.alphaBX; % convert QPD data from volts to nm
trapData.A_Ynm = (-trapData.A_Y)*calData.alphaAY; % convert QPD data from volts to nm
trapData.B_Ynm = (trapData.B_Y)*calData.alphaBY;  % convert QPD data from volts to nm

%% UPDATE DNA EXTENSION IN NANOMETERS
trapData.DNAext_X = trapData.TrapSep - trapData.A_Xnm - trapData.B_Xnm - calData.beadA/2 - calData.beadB/2;
trapData.DNAext_Y = trapData.TrapSep - trapData.A_Ynm - trapData.B_Ynm - calData.beadA/2 - calData.beadB/2;

%% UPDATE FORCE (pN)
trapData.force_AX = trapData.A_Xnm * calData.kappaAX; % multiply QPD data by trap stiffness
trapData.force_BX = trapData.B_Xnm * calData.kappaBX; % multiply QPD data by trap stiffness
trapData.force_AY = trapData.A_Ynm * calData.kappaAY; % multiply QPD data by trap stiffness
trapData.force_BY = trapData.B_Ynm * calData.kappaBY; % multiply QPD data by trap stiffness

end


