function plot_force_extension(trapData, construct, param)
% basic code to plot force extension curves given input of raw data for 
% force extension curve, type of DNA construct, and other trap parameters.

% WLC parameters
[WLC_param] = WLC_parameters;
hds = WLC_param.hds; % [nm/bp] interphosphate distance
hss = WLC_param.hss; % nm/nt
Pds = WLC_param.Pds; % nm
Pss = WLC_param.Pss; % nm
Sss = WLC_param.Sss; % pN
Sds = WLC_param.Sds; % pN
kT = WLC_param.kT;% Zhi's values (dsDNA)

    
    switch construct
            case 'fork'
                fork = 1;
                fit_first_relaxation = 1;
                hairpin = 0;
                dna1label = 'Fork Model';
                ssDNA1 = 4;
                dsDNA1 = 3105;
                fitoffset = 1;
                fitrange = [10 30]; % Force in pN
                fakeoffset = 0;
                stdF = [5 20]; % Forces for computing standard error of the residual
            case 'Hairpin-RH-dT-10'
                hairpin = 1;
                dna1label = 'Hairpin Closed Model';
                ssDNA1 = 10; %0;
                dsDNA1 = 3.055e3;
                dna2label = 'Hairpin Open Model';
                dsDNA2 = 3.055e3;
                ssDNA2 = 2*89+4+ssDNA1; %2*50+4+10; %
                fitoffset = 1;
                fitrange = [7 12]; % Force in pN defaul range is 7:12
                fakeoffset = 0;
                stdF = [2 10];
                stdF2 = [15 16];
            case 'RecA'
                fork = 1;
                fit_first_relaxation = 1;
                hairpin = 0;
                dna1label = 'Fork Model';
                ssDNA1 = 140;
                dsDNA1 = 3266;
                fitoffset = 1;
                fitrange = [10 20]; % Force in pN
                fakeoffset = 0;
                stdF = [5 20]; % Forces for computing standard error of the residual
                
                case 'T4'
                dsDNA1 = 3400;
                ssDNA1 = 0;
                dna1label = 'T4 Construct Model';  
                hairpin = 0;
                fork = 1;
                fitrange = [5 30];
                fakeoffset = 0;
    end
    
    if param.direction == 'X'
        F = (trapData.force_AX+trapData.force_BX)/2;
        DNAext = trapData.DNAext_X;

    elseif param.direction == 'Y'
        F = (trapData.force_AY+trapData.force_BY)/2;
        DNAext = trapData.DNAext_Y;
    end  


% Filter TrapSep
TrapSepFilt  = filter(ones(1,20)/20,1,trapData.TrapSep);  %changed from movmean by SPC on 2/21/2018

% SEPARATE UNFOLDING AND FOLDING
[numMAX, indMAX] = findpeaks(TrapSepFilt);
% indMAX = indMAX(2:end);
TrapSepInverse = [-max(numMAX) -TrapSepFilt -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
% indMIN = indMIN(2:end);
indMIN = indMIN-1;
rasterNUM = max([length(indMIN) length(indMAX)]);

trapData.time(indMAX)';
trapData.time(indMIN)';
indCOM = [];

    for ii = 1:1:length(indMAX)
       indCOM = [indCOM; trapData.time(indMIN(ii)) trapData.time(indMAX(ii)); trapData.time(indMAX(ii)) trapData.time(indMIN(ii+1))];
    end
    
DNA_ext_pull = DNAext(indMIN(1):indMAX(1));
DNA_ext_relax = DNAext(indMAX(1):end);
F_pull = F(indMIN(1):indMAX(1));
F_relax = F(indMAX(1):end);
    
% WLC PREDICTED DNA EXTENSION
    if hairpin
            fitforce = 1:0.5:20;
            % Closed hairpin
            dsdnaextcl = dsDNA1*hds*XWLCContour(fitforce,Pds,Sds,kT); % nm/bp
            ssdnaextcl = ssDNA1*hss*XWLCContour(fitforce,Pss,Sss,kT); % nm/bp
            totalextcl = dsdnaextcl + ssdnaextcl;
            % Open hairpin
            dsdnaextop = dsDNA2*hds*XWLCContour(fitforce,Pds,Sds,kT); % nm/bp
            ssdnaextop = ssDNA2*hss*XWLCContour(fitforce,Pss,Sss,kT); % nm/bp     % modified on 2/21/2018 SPC XWLCContour2b changed to XWLCCountour
            totalextop = dsdnaextop + ssdnaextop;
            
            M_DNA_fitrange =  mean(DNA_ext_pull(F_pull > fitrange(1) & F_pull < fitrange(2)));
            M_WLC_fitrange =  mean(totalextcl(fitforce > fitrange(1) & fitforce < fitrange(2)));
            fakeoffset = (M_WLC_fitrange - M_DNA_fitrange);
          %  fakeoffset = 0;
    elseif fork
            fitforce = 1:0.5:40;
            % Total fork extension
            dsdnaext = dsDNA1*hds*XWLCContour(fitforce,Pds,Sds,kT); % nm/bp
            ssdnaext = ssDNA1*hss*XWLCContour(fitforce,Pss,Sss,kT); % nm/bp
            totalext = dsdnaext + ssdnaext;
            M_DNA_fitrange =  mean(DNA_ext_pull(F_pull > fitrange(1) & F_pull < fitrange(2)));
            M_WLC_fitrange =  mean(totalext(fitforce > fitrange(1) & fitforce < fitrange(2)));
            fakeoffset = M_WLC_fitrange - M_DNA_fitrange;
          %add in stuff about fake offset some later time SPC 2/22/2018
    end
    
% get force offset
DNA_ext_pull = DNA_ext_pull + fakeoffset;
DNA_ext_relax = DNA_ext_relax + fakeoffset;


if param.overlay_fext == 1;
    figure(3);
%     if k < length(datasets)
%             hold on
%             plot(DNA_ext_pull, F_pull,'b')
%             hold on
%             plot(DNA_ext_relax, F_relax,'c')
%             hold on
%     elseif k == length(datasets)
%             hold on
%             plot(DNA_ext_pull, F_pull,'b')
%             hold on
%             plot(DNA_ext_relax, F_relax,'c')

hold on
plot(DNA_ext_pull, F_pull,'b')
hold on
plot(DNA_ext_relax, F_relax,'c')

%         if hairpin
%             hold on
%             plot(totalextcl, fitforce,'--k')
%             hold on
%             plot(totalextop, fitforce,'--r')
%         else
%             hold on
%             plot(totalext, fitforce, '--r')
%         end
        xlabel('DNA extension (nm)')
        ylabel('Force (pN)')
        title({['Overlaid Force vs Extension '] [construct]});
%         xlim([850 1150])
    end
    
if param.overlay_fext == 0
    figure
    plot(DNA_ext_pull, F_pull,'b')
    hold on
    plot(DNA_ext_relax, F_relax,'c')
        if hairpin
            hold on
            plot(totalextcl, fitforce,'--k')
            hold on
            plot(totalextop, fitforce,'--r')
        else
            hold on
            plot(totalext, fitforce, '--r')
        end
    xlabel('DNA extension (nm)')
    ylabel('Force (pN)')
    legend('DNA extension pull', 'DNA extension relax', 'WLC model closed', 'WLC model open')
    title({['Force vs Extension (' param.Date '\_' num2str(param.fextfnumber,'%03d') ')' ' , fakeoffset = ' num2str(round(fakeoffset,0)) ' nm']});
%     xlim([850 1150])
end

if param.model_hairpin_unwind == 1;
    [F_av,xtot,Gtot] = ModelHairpinUnwind(1,'TTTT',10);
    if param.overlay_fext == 1;
    figure(fext)
    hold on
    plot(xtot, F_av, 'Color',[0.4 0.4 0.4], 'LineWidth', 4)
%     legend('Hairpin Unwind Model')
    else
    hold on
    plot(xtot, F_av, 'Color',[0.4 0.4 0.4], 'LineWidth',4)
%     legend('Hairpin Unwind Model')
    end
    
end


end