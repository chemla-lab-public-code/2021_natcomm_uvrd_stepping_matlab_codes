% Here are my WLC parameters that I use for hairpine experiments
% in the UvrD Stepping Project

function [WLC_param] = WLC_parameters

% My hairpin WLC parameters
WLC_param.hds = 0.34; % [nm/bp] interphosphate distance 
WLC_param.hss = 0.59; % nm/nt
WLC_param.Pds = 53.0; % nm
WLC_param.Pss = 1.0; % nm
WLC_param.Sss = 1000; % pN
WLC_param.Sds = 1100; % pN
WLC_param.kT = 4.14;% Zhi's values (dsDNA)

end
