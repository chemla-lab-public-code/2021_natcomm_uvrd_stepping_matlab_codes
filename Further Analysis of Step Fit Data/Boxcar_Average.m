function [x_params, y_params] = Boxcar_Average(xdata,ydata,binwid,rem_option,plot_option)
% Function to calculate boxcar average for vectors of x, y data.  Error
% bars (both stdev and s.e.m.) are also calculated in both x and y.  There
% is an option to calculate the average and errors for the remainder, and
% there is also a plotting option.

y_params.ave = [];
y_params.std = [];   % initialize variables
y_params.sem = [];

x_params.ave = [];
x_params.std = [];
x_params.sem = [];

% [x_sort, x_indx] = sort(xdata);  % sorts xdata in order, then corresponding ydata
[x_sort, x_indx] = sort(abs(xdata)); % better option, sorts absolute value of data, in case one of the 
                                     %  has all negative numbers, like in
y_sort = ydata(x_indx);              % the case of negative step sizes.

size = length(ydata);

num = fix(size/binwid);  

c = rem(size,binwid);

for i = 1:1:num         % calculates average, stdev, s.e.m., in both x and y for each grouping of the boxcar average
                                  
    ymean = mean(y_sort((binwid*(i-1)+1):(binwid*i)));
    ysted = std(y_sort((binwid*(i-1)+1):(binwid*i)));
    ysem = (ysted)/(sqrt(binwid));
   
    
    xmean = mean(x_sort((binwid*(i-1)+1):(binwid*i)));
    xsted = std(x_sort((binwid*(i-1)+1):(binwid*i)));
    xsem = (xsted)/(sqrt(binwid));
    
    y_params.ave = [y_params.ave; ymean];
    y_params.std = [y_params.std; ysted];
    y_params.sem = [y_params.sem; ysem];
    
    x_params.ave = [x_params.ave; xmean];
    x_params.std = [x_params.std; xsted];
    x_params.sem = [x_params.sem; xsem];
    
end

if rem_option == 1       % calculates average and errors for the remainder, if that option is chosen
                          
    ymean_rem = mean(y_sort(((binwid*num)+1):end));
    ysted_rem = std(y_sort(((binwid*num)+1):end));
    ysem_rem = (ysted_rem)/(sqrt(c));
    
    xmean_rem = mean(x_sort(((binwid*num)+1):end));
    xsted_rem = std(x_sort(((binwid*num)+1):end));
    xsem_rem = (xsted_rem)/(sqrt(c));
    
    y_params.ave = [y_params.ave; ymean_rem];
    y_params.std = [y_params.std; ysted_rem];
    y_params.sem = [y_params.sem; ysem_rem];
    
    x_params.ave = [x_params.ave; xmean_rem];
    x_params.std = [x_params.std; xsted_rem];
    x_params.sem = [x_params.sem; xsem_rem];
    
end

% x_params.ave = -1*x_params.ave;  % IMPORTANT!! When dealing with data where
% the values of the x vector are negative numbers (like for negative,
% re-zipping step sizes, line 67 needs to be uncommented.

x_params.weight = 1/((x_params.sem).^2); % calculates weights based ons standard error, for fitting

y_params.weight = 1/((y_params.sem).^2);

if plot_option == 1        % plotting
                
     figure
     plot(xdata,ydata,'r*')
     hold on                     % different options for plotting - line 80 is x,y averages, with y errors, 81 is with both x and y errors, and 82 is without x or y errors
     errorbar(x_params.ave, y_params.ave, y_params.sem, 'o', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'g', 'MarkerSize', 10, 'Color', 'k', 'linew', 1)
%      errorbar(x_params.ave, y_params.ave, y_params.sem, y_params.sem, x_params.std, x_params.std, 'o', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'g', 'MarkerSize', 10, 'Color', 'k', 'linew', 1)
%       plot(x_params.ave, y_params.ave,'b*')
% 
    xlabel('x data')
    ylabel('y data')
    title('Box car average plot')

datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;
name_save = 'Boxcar average plot';  % save figure
saveas(gca, fullfile(fpath_save, name_save));

end


end


