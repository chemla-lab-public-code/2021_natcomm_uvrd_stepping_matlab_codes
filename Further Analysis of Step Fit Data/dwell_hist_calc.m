function [dwell_stats] = dwell_hist_calc(dwell_data,lim) % 3/6/2019
% Automates process of constructing dwell time histogram.  Calculates
% average, standard deviation, sem, binsize, etc., puts in structure
% variable called dwell stats. Bin size is calculated by Freedman 
% Diaconis_rule. Lim is the lower limit on dwell size. Can also be used
% to get histogram of velocity or really any other quantity, in that case
% you want to set the lower limit to zero.

dwell_stats.dwells = dwell_data(dwell_data >= lim);

dwell_stats.ave = mean(dwell_stats.dwells);

dwell_stats.stdev = std(dwell_stats.dwells);  % calculates statistics

dwell_stats.size = length(dwell_stats.dwells);

dwell_stats.sem = (dwell_stats.stdev)/(sqrt(dwell_stats.size));

dwell_stats.nmin = (((dwell_stats.ave)^2)/((dwell_stats.stdev)^2));

dwell_stats.nmin_err = error_prop_nmin(dwell_stats.ave,dwell_stats.stdev,dwell_stats.sem,dwell_stats.nmin,dwell_stats.size);
                                          % calculates nmin error
dwell_sort = sort(dwell_stats.dwells);

N = length(dwell_sort);

if rem(N,2) == 0
    med1 = median(dwell_sort(1:(N)/2));
    med2 = median(dwell_sort(((N/2) + 1):N));
elseif rem(N,2) ~= 0
    a = N - 1;
    b = a/2;
    med1 = median(dwell_sort(1:b));
    med2 = median(dwell_sort((b + 2):N));
end    

dwell_stats.iqr = med2 - med1;

denom = (N)^(1/3);
      
 dwell_stats.binsize = (2*dwell_stats.iqr)/(denom); % bin size calculated by Freedman Diaconis rule

%  dwell_stats.binsize = 0.1635;  % alternatively, can set binsize to
%  arbitrary value.

rg = range(dwell_sort);

dwell_stats.binnum = round((rg)/(dwell_stats.binsize));

% [dwell_stats.dwell_c, dwell_stats.edges] = histcounts(dwell_stats.dwells,0.02:dwell_stats.binsize:(max(dwell_stats.dwells) + 0.3));
[dwell_stats.dwell_c, dwell_stats.edges] = histcounts(dwell_stats.dwells,min(dwell_stats.dwells):dwell_stats.binsize:(max(dwell_stats.dwells) + 0.3));
dwell_stats.xvals = (dwell_stats.edges(2:end) + dwell_stats.edges(1:end-1))/2;
dwell_stats.prob = dwell_stats.dwell_c/sum(dwell_stats.dwell_c);
dwell_stats.pdf = dwell_stats.prob/dwell_stats.binsize;  % calcules stuff for histogram

figure
hold on
                      % plots histogram
 histogram(dwell_stats.dwells, (min(dwell_stats.dwells):dwell_stats.binsize:(max(dwell_stats.dwells) + 0.3)),'Normalization','count')
%  histogram(dwell_stats.dwells,(min(dwell_stats.dwells):dwell_stats.binsize:max(dwell_stats.dwells)+0.1),'Normalization','pdf')
xlabel('Dwell time (s)')
ylabel('Count (s)')
title('dwell hist') % this can vary a a decent amount from time to time

text(0, max(dwell_stats.dwell_c)*0.8, ['ave = ' num2str(dwell_stats.ave,'%0.3f') ' N = ' num2str(N,'%0.3f') ' '])
text(0, max(dwell_stats.dwell_c)*0.5, ['s.d. = ' num2str(dwell_stats.stdev,'%0.3f') ' s.e.m. =' num2str(dwell_stats.sem,'%0.3f') ' '])
text(0, max(dwell_stats.dwell_c)*0.2, ['nmin. = ' num2str(dwell_stats.nmin,'%0.3f') ' nmin err =' num2str(dwell_stats.nmin_err,'%0.3f') ' '])

datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;
name_save = 'dwell hist.fig';
saveas(gca, fullfile(fpath_save, name_save));

end


