function [resamp_cut_low, counts_low,resamp_cut_high, counts_high, diff] = Bootstrap_dwell_add_on_plotting(low_range, high_range,resamp,dwell_size )
%SPC_Bootstrap_add_on_plotting: short function designed to plot main
%results from bootstrap add on, specifically how often each dwell in the
%vector of dwell times showes up in the two distinct populations of
%bootstrapped resampled dwell vectors (defined by the range of their nmin
%values), and also what is the difference in the frequency of each dwell
%between the two populations

[resamp_cut_low, counts_low] = Bootstrap_dwell_add_on(low_range(1), low_range(2), resamp, dwell_size);   % count how often each dwell shows up in the two populations

[resamp_cut_high, counts_high] = Bootstrap_dwell_add_on(high_range(1), high_range(2), resamp, dwell_size); % 

diff.low_minus_high = counts_low.dwell_tot - counts_high.dwell_tot; % calculate difference in the frequency of each dwell's appearance between the two populations

diff.high_minus_low = counts_high.dwell_tot - counts_low.dwell_tot;

figure
plot(counts_low.dwell_num, counts_low.dwell_tot,'r*');
hold on                                                    % plot the results
plot(counts_high.dwell_num, counts_high.dwell_tot,'b*');
plot(counts_low.dwell_num, diff.low_minus_high,'k*');
plot(counts_high.dwell_num, diff.high_minus_low,'m*');
xlabel('dwell number')
ylabel('bootstrap frequency')
title('bb 1 uM ATP outlier plot')
legend('low range counts','high range counts', 'low minus high', 'high minus low')

datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;
name_save = 'bb small steps high ATP v2 outlier plot';  % save figure
saveas(gca, fullfile(fpath_save, name_save));

end

