function [rp_error,nmin_error] = rp_plus_nmin_error(y_mean,y_stdev,y_sec_mm,y_sec_mm_stdev,numpoints,y_rp,y_nmin)
% Alternate way to calculate the error on nmin, also calculates error on
% randomness parameter rp. Be sure to always use this code in conjunction
% with Boxcar_Average_nmin and Boxcar_Average_nmin_custom_range

stdev_y_rp = y_rp*sqrt((y_sec_mm_stdev./y_sec_mm).^2 + (2*y_stdev./y_mean).^2);

stdev_y_nmin = (stdev_y_rp./y_rp).*y_nmin;

rp_error = stdev_y_rp/sqrt(numpoints);

nmin_error = stdev_y_nmin/sqrt(numpoints);

end

