function [tv, pgauss, kern] = kernel_density_plot(steps, step_err)
% short function that uses kernel density to estimate probability density
% of step sizes given the array of step sizes and the error on each step.
% Main difference between this function and kernel_density_1D is that this
% is one plots kernel density estimate in the end. Can be adapted for 
% things other than step sizes.

pgauss = [];
        wid = 0.01; 
%           tv = min(steps-3*step_err):wid:max(steps+3*step_err); 
               tv = 0:wid:max(steps+3*step_err); % test variable
%            tv = min(steps-3*step_err):wid:0; 

% can choose different defitions for domain of kernel density estimate
% (i.e.,all positve steps, all negative steps, or both


        for gg = 1:length(steps)
            pgauss(:,gg) = 1/(step_err(gg)*sqrt(2*pi)).*exp(-(tv'-steps(gg)).^2/(2*step_err(gg)^2));
        end
        kern = sum(pgauss,2);  % kernel density calculation
        
        %yyaxis right
%             figure            % plotting the kernel density estimate
        plot(tv, kern/sum(kern)/wid,'r','linew',2)
       % set(gca,'YColor','r')
        xlabel('step size (bp)')
        ylabel('Probability Density')
        title('Kernel density estimate')
        
%         % Find peaks
%         y1 = kern(1:end-1);
%         y2 = kern(2:end);
%         x1 = tv(1:end-1)';             % Ignore anything from line 31
%         x2 = tv(2:end)';               % downward
%          
%         dr = (y2-y1)./(x2-x1);
%         xdr = x2-(tv(2)-tv(1))/2;
%         
%         meanind = 1;
%         tt = 1;
%         steppedUp = [];
%         transthresh = 0.04;
%         while tt <= length(dr)
%             indAboveCut = 1;
%             allAboveCut = [];
%             while length(dr) >= tt && dr(tt) > transthresh % Steps up
%                 allAboveCut(indAboveCut) = tt;
%                 tt = tt + 1;
%                 indAboveCut = indAboveCut + 1;
%             end
%             if ~isempty(allAboveCut)
%                 avedifflocal = zeros(1,length(dr));
%                 avedifflocal(allAboveCut) = dr(allAboveCut);
%                 [~, steppedUp(meanind)] = max(avedifflocal);
%                 meanind = meanind + 1;
%             else
%                 tt = tt + 1;
%             end
%         end
%         steppedUp(steppedUp==0) = [];
        
end
   