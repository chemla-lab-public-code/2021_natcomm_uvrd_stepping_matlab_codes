function [resamp, tv, kern_mat, histmat] = Bootstrap_kernel_density(steps,step_err)
%Function to do bootstrap resampling from step size distribution to create
%a bootstrapped kernel density estimate, to confirm the statistical
%significance of peaks in the kernel density estimate. Takes the vector of
%steps and the vector step standard errors as inputs.  Calles kd_bootstrap
%to do the individual kernel density estimate

steps_pos = steps(steps > 0);                 % separate into positive and
steps_neg = steps(steps < 0);                 % negative steps

step_sem_pos = step_err(steps > 0);
step_sem_neg = step_err(steps < 0);

wid.pos = 0.01;
tv.pos = 0:wid.pos:max(steps_pos + 3*step_sem_pos);  % initialize variables, here it is 
                     % the relevant domain for positive and  negative steps
wid.neg = 0.01;                                       
tv.neg = min(steps_neg - 3*step_sem_neg):wid.neg:0;

kern_mat.pos = zeros(length(tv.pos),10000);  % will do 10,000 resamples
kern_mat.neg = zeros(length(tv.neg),10000);

a = length(steps_pos);
b = length(steps_neg);

resamp.index_pos = zeros(a,10000); 
resamp.index_neg = zeros(b,10000);  % initialize variables for resampling data

resamp.steps_rs_pos = zeros(a,10000); 
resamp.steps_rs_neg = zeros(b,10000);

resamp.step_sem_rs_pos = zeros(a,10000);
resamp.step_sem_rs_neg = zeros(b,10000);

for i = 1:1:10000                               % Main loop that does the bootstrap analysis.  For the time being, do 1000
                                                % bootstrap samples.                               
    indx_pos = randi([1 a],a,1);                
    indx_neg = randi([1 b],b,1);
    
    resamp.index_pos(:,i) = indx_pos; 
    resamp.index_neg(:,i) = indx_neg;
    
    resamp.steps_rs_pos(:,i) = steps_pos(indx_pos);
    resamp.steps_rs_neg(:,i) = steps_neg(indx_neg);
    
    resamp.step_sem_rs_pos(:,i) = step_sem_pos(indx_pos);
    resamp.step_sem_rs_neg(:,i) = step_sem_neg(indx_neg);
                                                           % First, generate resample of
    steps_pos_v2 = steps_pos(indx_pos);                    % random indices
    steps_neg_v2 = steps_neg(indx_neg);
    
    step_sem_pos_v2 = step_sem_pos(indx_pos);
    step_sem_neg_v2 = step_sem_neg(indx_neg);
    
    [~, kern_rs_pos] = kd_bootstrap(tv.pos, steps_pos_v2, step_sem_pos_v2);
    [~, kern_rs_neg] = kd_bootstrap(tv.neg, steps_neg_v2, step_sem_neg_v2);  % do kernel density estimate for each resampled step size
                                                                                % distribution
    
    amp_pos = kern_rs_pos/sum(kern_rs_pos)/wid.pos;
    amp_neg = kern_rs_neg/sum(kern_rs_neg)/wid.neg;
    
    kern_mat.pos(:,i) = amp_pos;
    kern_mat.neg(:,i) = amp_neg;
    
end

prob_range = 0:0.01:1;

prob_range_val = (prob_range(2:end)+prob_range(1:end-1))/2;        % Initialize variables for computing distribution of kernel density distributions

histmat.pos = zeros(length(tv.pos),length(prob_range_val));

histmat.neg = zeros(length(tv.neg),length(prob_range_val));

for j = 1:1:length(tv.pos)
    
    [h_pos_c] = histcounts(kern_mat.pos(j,:),prob_range);          % calculate distributions of positive and negative kernel densities
    
    histmat.pos(j,:) = h_pos_c/sum(h_pos_c);
    
end

for k = 1:1:length(tv.neg)
    
    [h_neg_c] = histcounts(kern_mat.neg(k,:),prob_range);
    
    histmat.neg(k,:) = h_neg_c/sum(h_neg_c);
    
end

%     datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\'; % set up place where you want to save the data
% 
%     figure
    
% [X_pos, Y_pos] = meshgrid(tv.pos,prob_range_val);  % optional part to
% disply distribution of bootstrapped kernel density estimates as either a
% heat mapy or contour plot.
% 
% [X_neg, Y_neg] = meshgrid(tv.neg,prob_range_val);
% 
%     
%     surf(X_pos, Y_pos, histmat.pos', 'FaceColor', 'interp', 'EdgeColor', 'interp');
%     hold on
%     surf(X_neg, Y_neg, histmat.neg', 'FaceColor', 'interp', 'EdgeColor', 'interp');
%     view(gca,[0,90]);
%     zlim([0 0.3])                                                                   % Display data as a heat map
%     caxis([0 0.1])
%     colorbar;
%     xlabel('step size (bp)')
%     ylabel('Probability Density')
%     title('Heat map of 5 uM dimer steps') 
%     
%     fpath_save = datadirectory;                                                           % save heat map
%     name_save = 'Heat Map of 5 uM dimer steps sem kernel density estimates 10000';  
%     saveas(gca, fullfile(fpath_save, name_save));
% 
%                                                                                    % Display data as a contour plot
%     figure        
%     contour(X_pos, Y_pos, histmat.pos')
%     contour(X_pos, Y_pos, histmat.pos', (0.01:0.01:1.0))
%     hold on
%     contour(X_neg, Y_neg, histmat.neg')
%     contour(X_neg, Y_neg, histmat.neg', (0.01:0.01:1.0))
%     colorbar;
%     xlabel('step size (bp)')
%     ylabel('Probability Density')
%     title('Contour plot 5 uM dimer steps') 
% 
%     fpath_save = datadirectory;                                                                     % save contour plot
%     name_save = 'Contour plot of 5 uM dimer steps sem kernel density estimates 10000';  
%     saveas(gca, fullfile(fpath_save, name_save));
%     

 
end