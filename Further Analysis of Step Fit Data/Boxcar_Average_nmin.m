function [x_params, y_params] = Boxcar_Average_nmin(xdata,ydata,binwid,rem_option,plot_option)
% Calculates boxcar average for vectors of x and y data like
% Boxcar_Average, but also calculates nmin (mean squared over variance) for
% y data.  Thus, y data should be dwell times when being used with theis
% function.  The error on nmin is calculated by propagating the errors from
% the mean and second moment of dwell time.  Like before averages and
% errors (stdev and s.e.m.) are calculated for x and y as wel

y_params.ave = [];
y_params.std = [];   % initialize variables
y_params.sem = [];
y_params.nmin = []; 
y_params.second_moment_ave = [];
y_params.second_moment_stdev = [];
y_params.second_moment_sem = [];
y_params.nmin_err = [];
y_params.rp = []; % randomness parameter (inverse of nmin) added by SPC on 10/29/2020
y_params.rp_err = []; % error on randomness paraemeter added by SPC on 10/29/2020

x_params.ave = [];
x_params.std = [];
x_params.sem = [];

%  [x_sort, x_indx] = sort(xdata);% sorts xdata in order, then corresponding ydata

 [x_sort, x_indx] = sort(abs(xdata)); % better to sort based on absolute value, in case the x values are negative
                                      % like for negative re-zipping step
y_sort = ydata(x_indx);               % sizes.

size = length(ydata);

num = fix(size/binwid);  

c = rem(size,binwid);

for i = 1:1:num  % calculates average, error, s.e.m., nmin, rp, and nmin and rp errors for each bin
    
    ymean = mean(y_sort((binwid*(i-1)+1):(binwid*i)));
    ysted = std(y_sort((binwid*(i-1)+1):(binwid*i)));
    ysem = (ysted)/(sqrt(binwid));
    y_sec_mm_av = mean((y_sort((binwid*(i-1)+1):(binwid*i))).^2);
    y_sec_mm_stdev = std((y_sort((binwid*(i-1)+1):(binwid*i))).^2);
    y_sec_mm_sem = (y_sec_mm_stdev)/(sqrt(binwid));
    y_nmin = (ymean.^2)/(y_sec_mm_av - (ymean.^2));
    y_rp = 1./y_nmin; % added by SPC on 10/29/2020
    [y_rp_err, y_nmin_err] = rp_plus_nmin_error(ymean,ysted,y_sec_mm_av,y_sec_mm_stdev,binwid,y_rp,y_nmin);
 % added by SPC on 10/29/2020 
 % Important: be sure to always use rp_plus_nmin_error to calculate
 % nmin error with this code.
    
    
    xmean = mean(x_sort((binwid*(i-1)+1):(binwid*i)));
    xsted = std(x_sort((binwid*(i-1)+1):(binwid*i)));
    xsem = (xsted)/(sqrt(binwid));
    
    y_params.ave = [y_params.ave; ymean];  % compiles values from each run through for loop
    y_params.std = [y_params.std; ysted];
    y_params.sem = [y_params.sem; ysem];
    y_params.second_moment_ave = [y_params.second_moment_ave; y_sec_mm_av];
    y_params.second_moment_stdev = [y_params.second_moment_stdev; y_sec_mm_stdev];
    y_params.second_moment_sem = [y_params.second_moment_sem; y_sec_mm_sem];
    y_params.nmin = [y_params.nmin; y_nmin];
    y_params.rp = [y_params.rp; y_rp];
    y_params.rp_err = [y_params.rp_err; y_rp_err]; 
    y_params.nmin_err = [y_params.nmin_err; y_nmin_err];  
    x_params.ave = [x_params.ave; xmean];
    x_params.std = [x_params.std; xsted];
    x_params.sem = [x_params.sem; xsem];
    
end

if rem_option == 1  % Calculates statistics for last bin for remainder
    
    ymean_rem = mean(y_sort(((binwid*num)+1):end));
    ysted_rem = std(y_sort(((binwid*num)+1):end));
    ysem_rem = (ysted_rem)/(sqrt(c));
    y_sec_mm_av_rem = mean((y_sort(((binwid*num)+1):end)).^2);
    y_sec_mm_stdev_rem = std((y_sort(((binwid*num)+1):end)).^2);
    y_sec_mm_sem_rem = (y_sec_mm_stdev_rem)/(sqrt(c));
    ynmin_rem = (ymean_rem).^2/(y_sec_mm_av_rem - ymean_rem.^2);
    y_rp_rem = 1./ynmin_rem;
    [y_rp_err_rem, ynmin_err_rem] = rp_plus_nmin_error(ymean_rem,ysted_rem,y_sec_mm_av_rem,y_sec_mm_stdev_rem,c,y_rp_rem,ynmin_rem);
    % Again, always use rp_plus_nmin_error to calculate error on nmin
    
    xmean_rem = mean(x_sort(((binwid*num)+1):end));
    xsted_rem = std(x_sort(((binwid*num)+1):end));
    xsem_rem = (xsted_rem)/(sqrt(c));
    
    y_params.ave = [y_params.ave; ymean_rem]; % comoiles results from remainder with the rest of the statistics
    y_params.std = [y_params.std; ysted_rem];
    y_params.sem = [y_params.sem; ysem_rem];
                                           
    y_params.second_moment_ave = [y_params.second_moment_ave; y_sec_mm_av_rem];
    y_params.second_moment_stdev = [y_params.second_moment_stdev; y_sec_mm_stdev_rem];
    y_params.second_moment_sem = [y_params.second_moment_sem; y_sec_mm_sem_rem];
    y_params.nmin = [y_params.nmin; ynmin_rem];
    y_params.rp = [y_params.rp; y_rp_rem];
    y_params.nmin_err = [y_params.nmin_err; ynmin_err_rem];
    y_params.rp_err = [y_params.rp_err; y_rp_err_rem];
    
    x_params.ave = [x_params.ave; xmean_rem];
    x_params.std = [x_params.std; xsted_rem];
    x_params.sem = [x_params.sem; xsem_rem];
    
    
end

% x_params.ave = -1*x_params.ave; IMPORTANT: uncomment line 108 if your x
% values are negative, as is the case for negative, re-zipping step sizes

x_params.weight = 1/((x_params.sem).^2); % calculates weights for fits

y_params.weight = 1/((y_params.sem).^2);

if plot_option == 1
    
    figure
    plot(xdata,ydata,'r*')  
    hold on
%   errorbar(x_params.ave, y_params.nmin, y_params.nmin_err, 'o', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'g', 'MarkerSize', 10, 'Color', 'k', 'linew', 1)
    errorbar(x_params.ave, y_params.nmin, y_params.nmin_err, y_params.nmin_err, x_params.std, x_params.std, 'o', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'g', 'MarkerSize', 10, 'Color', 'k', 'linew', 1)
    xlabel('step size (bp)')  % a few different plotting options, line 120 just x ave, y ave, y error, line 121 gives x error as well
    ylabel('Nmin')
    title('Boxcar average nmin')

datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;
name_save = 'Boxcar average nmin';  % save figure
saveas(gca, fullfile(fpath_save, name_save));


end


end


