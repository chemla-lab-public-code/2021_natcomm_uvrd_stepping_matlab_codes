function [x_params, y_params] = Boxcar_average_custom_range(xdata, ydata, midrange) 
% 4/21/2019, sort of similar to Boxcar_Average function, but uses a more customized 
% binning range.  Instead of bins of defined size, the x-range intervals
% for binning are defined as inputs. The midrange input is a vector that
% defines a series of equally spaced bins, while the first and last bins
% correspond to all data points which come before the start of midrange and
% after the end of it.  Like Boxcar_Average, the function calculates
% average,stdev, and s.e.m. for both x and y, with options to calculate
% statistics for the remainder and diplay the plots at the end.  This
% function is useful for plotting average step size and dwell time versus
% force in pN.


y_params.ave_mid = [];
y_params.std_mid = [];   % initialize variables
y_params.sem_mid = [];

x_params.ave_mid = [];
x_params.std_mid = [];
x_params.sem_mid = [];

xdata_round = round(xdata);

a = length(midrange);

for i = 1:1:a                % get x and y average, stdev, sem, for each bin defined by midrange
    
    xdat_temp = xdata(xdata_round == midrange(i));
    
    ydat_temp = ydata(xdata_round == midrange(i));
    
    xmean = mean(xdat_temp);
    
    xstdev = std(xdat_temp);
    
    xsem = xstdev/sqrt(length(xdat_temp));
    
    ymean = mean(ydat_temp);
    
    ystdev = std(ydat_temp);
    
    ysem = ystdev/sqrt(length(ydat_temp));
    
    y_params.ave_mid = [y_params.ave_mid; ymean];
    y_params.std_mid = [y_params.std_mid; ystdev];
    y_params.sem_mid = [y_params.sem_mid; ysem];
    
    x_params.ave_mid = [x_params.ave_mid; xmean];
    x_params.std_mid = [x_params.std_mid; xstdev];
    x_params.sem_mid = [x_params.sem_mid; xsem];
    
end
    
xdat_left = xdata(xdata_round < midrange(1)); % get statistics for outermost bins

ydat_left = ydata(xdata_round < midrange(1));

x_params.ave_left = mean(xdat_left);
x_params.std_left = std(xdat_left);
x_params.sem_left = x_params.std_left/sqrt(length(xdat_left));

y_params.ave_left = mean(ydat_left);
y_params.std_left = std(ydat_left);
y_params.sem_left = y_params.std_left/sqrt(length(ydat_left));

xdat_right = xdata(xdata_round > midrange(end));

ydat_right = ydata(xdata_round > midrange(end));

x_params.ave_right = mean(xdat_right);
x_params.std_right = std(xdat_right);
x_params.sem_right = x_params.std_right/sqrt(length(xdat_right));

y_params.ave_right = mean(ydat_right);
y_params.std_right = std(ydat_right);
y_params.sem_right = y_params.std_right/sqrt(length(ydat_right));     % combine data for outermost bins with data for middle bins

x_params.ave_tot = vertcat(x_params.ave_left, x_params.ave_mid, x_params.ave_right);
x_params.std_tot = vertcat(x_params.std_left, x_params.std_mid, x_params.std_right);
x_params.sem_tot = vertcat(x_params.sem_left, x_params.sem_mid, x_params.sem_right);

y_params.ave_tot = vertcat(y_params.ave_left, y_params.ave_mid, y_params.ave_right);
y_params.std_tot = vertcat(y_params.std_left, y_params.std_mid, y_params.std_right);
y_params.sem_tot = vertcat(y_params.sem_left, y_params.sem_mid, y_params.sem_right);
    

    
    figure
    plot(xdata,ydata,'r*')
    hold on
    errorbar(x_params.ave_tot, y_params.ave_tot, y_params.sem_tot, 'o', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'g', 'MarkerSize', 10, 'Color', 'k', 'linew', 1)
 
    xlabel('xdata')
    ylabel('ydata') 
    title('Boxcar average custom range')
    
datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;
name_save = 'Boxcar average custom range';  % save figure
saveas(gca, fullfile(fpath_save, name_save));


end

