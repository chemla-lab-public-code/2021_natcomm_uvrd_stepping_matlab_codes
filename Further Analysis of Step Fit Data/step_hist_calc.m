function [step_stats] = step_hist_calc(step_mat) % 3/6/2019
% Automates process of constructing size histogram.  Calculates average, sem, 
% standard deviation, s.e.m., binsize, etc and stores in the structure
% variable step_stats.  Separates step data into positive and negative
% steps.  Bin size is calculated by Freedman Diaconis_rule.

step_mat_pos = step_mat(step_mat(:,5) > 0,:);

step_mat_neg = step_mat(step_mat(:,5) < 0,:);  % separate into positive and negative steps

step_stats.pos = step_mat_pos(:,5);
step_stats.neg = step_mat_neg(:,5);

step_stats.pos_ave = mean(step_stats.pos);
step_stats.neg_ave = mean(step_stats.neg);  % calculate mean, stdev, sem

step_stats.pos_stdev = std(step_stats.pos);
step_stats.neg_stdev = std(step_stats.neg);

step_stats.pos_size = length(step_stats.pos);
step_stats.neg_size = length(step_stats.neg);

step_stats.pos_sem = (step_stats.pos_stdev)/(sqrt(length(step_stats.pos)));
step_stats.neg_sem = (step_stats.neg_stdev)/(sqrt(length(step_stats.neg)));

pos_sort = sort(step_stats.pos);
neg_sort = sort(step_stats.neg.*-1);

N_pos = length(pos_sort);
N_neg = length(neg_sort);

if rem(N_pos,2) == 0
    pos_med1 = median(pos_sort(1:(N_pos)/2));
    pos_med2 = median(pos_sort(((N_pos/2) + 1):N_pos));
elseif rem(N_pos,2) ~= 0
    pos_a = N_pos - 1;
    pos_b = pos_a/2;
    pos_med1 = median(pos_sort(1:pos_b));
    pos_med2 = median(pos_sort((pos_b + 2):N_pos));
end    

if rem(N_neg,2) == 0
    neg_med1 = median(neg_sort(1:(N_neg)/2));
    neg_med2 = median(neg_sort(((N_neg/2) + 1):N_neg));
elseif rem(N_neg,2) ~= 0
    neg_a = N_neg - 1;
    neg_b = neg_a/2;
    neg_med1 = median(neg_sort(1:neg_b));
    neg_med2 = median(neg_sort((neg_b + 2):N_neg));
end    

step_stats.pos_iqr = pos_med2 - pos_med1;  % 

pos_denom = (N_pos)^(1/3);

 step_stats.pos_binsize = (2*step_stats.pos_iqr)/(pos_denom);  % vinsize calculated via Freedman-Diaconis rule

% step_stats.pos_binsize = 0.4302;  % alternatively,can just set binsize to
% arbitrary value

range_pos = range(pos_sort);

step_stats.pos_binnum = round((range_pos)/(step_stats.pos_binsize));

step_stats.neg_iqr = neg_med2 - neg_med1;

neg_denom = (N_neg)^(1/3);

step_stats.neg_binsize = (2*step_stats.neg_iqr)/(neg_denom); % calculates binsize via Freedman-Diaconis rule
% step_stats.neg_binsize = 1.3290;  % alternatively, can choose arbitrary
% value for binsize

range_neg = range(neg_sort);

step_stats.neg_binnum = round((range_neg)/(step_stats.neg_binsize));

[step_stats.stepc_pos, step_stats.edges_pos] = histcounts(step_stats.pos,0:step_stats.pos_binsize:9.0);
[step_stats.stepc_neg, step_stats.edges_neg] = histcounts(step_stats.neg,-9.0:step_stats.neg_binsize:0.0); % calculates important values for histogram
step_stats.xvals_pos = (step_stats.edges_pos(2:end) + step_stats.edges_pos(1:end-1))/2;
step_stats.xvals_neg = (step_stats.edges_neg(2:end) + step_stats.edges_neg(1:end-1))/2;
step_stats.prob_pos = step_stats.stepc_pos/sum(step_stats.stepc_pos);
step_stats.prob_neg = step_stats.stepc_neg/sum(step_stats.stepc_neg);
step_stats.pdf_pos = step_stats.prob_pos/step_stats.pos_binsize;
step_stats.pdf_neg = step_stats.prob_neg/step_stats.neg_binsize;

max_pos = max(step_stats.stepc_pos);
max_neg = max(step_stats.stepc_neg);

max_tot = vertcat(max_pos, max_neg);

figure
hold on
                         % plotting histograms
 histogram(step_stats.pos, 0:step_stats.pos_binsize:9.0,'Normalization','count') 
% histogram(step_stats.pos, 0:step_stats.pos_binsize:max(step_stats.pos),'Normalization','pdf') 
xlabel('Step Size (bp)')
ylabel('counts')
title('Unwinding and re-zipping step hist') % this can vary a a decent amount from time to time

text(0, max(max_tot)*0.8, ['ave = ' num2str(step_stats.pos_ave,'%0.2f') ' N = ' num2str(N_pos,'%0.2f') ' '])
text(0, max(max_tot)*0.5, ['s.d. = ' num2str(step_stats.pos_stdev,'%0.2f') ' s.e.m. =' num2str(step_stats.pos_sem,'%0.2f') ' '])

hold on
histogram(step_stats.neg, -9.0:step_stats.neg_binsize:0,'Normalization','count') % changed to normalization and pdf 7/4/2019
%  histogram(step_stats.neg, min(step_stats.neg):step_stats.neg_binsize:0,'Normalization','pdf') % changed to normalization and pdf 7/4/2019


text(-9.0, max(max_tot)*0.8, ['ave = ' num2str(step_stats.neg_ave,'%0.2f') ' N = ' num2str(N_neg,'%0.2f') ' '])
text(-9.0, max(max_tot)*0.5, ['s.d. = ' num2str(step_stats.neg_stdev,'%0.2f') ' s.e.m. = ' num2str(step_stats.neg_sem,'%0.2f') ' '])


datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\';

fpath_save = datadirectory;  % save data
name_save = 'Unwinding and re-zipping step hist';
saveas(gca, fullfile(fpath_save, name_save));
% 
end

