function [backstep_mat_rev, backstep_stats_rev] = Backstep_prob_calc(stepmat)
% Function to calculate number and probability of backsteps given a matrix
% of step data.  Identifies when two (or more) successive unwindings steps
% are interrupted by a negative step for unwinding backsteps, and when a
% positive step interrupts successive re-zipping steps for re-zipping
% backsteps.  Outputs both statistics for the backsteps and a matrix of the
% identified backsteps and their associated data.

backstep_mat_rev = [];
[~, ind] = unique(stepmat(:,1:4),'rows');
ind = sort(ind);                            % initialize variables
lngth_tot = size(stepmat,1);
ind = vertcat(ind, lngth_tot + 1);

pos_stepmat = stepmat(stepmat(:,5) > 0,:);
neg_stepmat = stepmat(stepmat(:,5) < 0,:);

backstep_stats_rev.pos_steps_tot = size(pos_stepmat,1);  % identify total number of positive and negative steps
backstep_stats_rev.neg_steps_tot = size(neg_stepmat,1);

for i = 1:1:(length(ind) - 1)
    
    steps_temp = stepmat(ind(i):(ind(i+1))-1,:);  % focus on one fitted burst at a time
    steps_only = steps_temp(:,5);
    steps_log = zeros(length(steps_only),1);
    
    for j = 1:1:length(steps_only)
        
        if steps_only(j) > 0
            steps_log(j) = 1;   % classify forward and backward steps in selected bursts
        end                     % forward (positive) steps assigned value of 1, backward (negative) steps assigned value of 0
                                % IMPORTANT: lines 29-35 are for the case
        if steps_only(j) < 0    % of backsteps in unwinding, need to comment these and uncomment 37-44 if you are looking 
            steps_log(j) = 0;   % for backsteps in re-zipping
        end
        
%         if steps_only(j) > 0    % use this part if looking at backsteps
%             steps_log(j) = 0;   % in re-zipping
%             
%         end
%         
%         if steps_only(j) < 0
%             steps_log(j) = 1;
%         end
    end
    
        backstep_seq = [1 0 1];  % search specifically for forward-backward-forward sequence
    
        backstep_ind_st = strfind(steps_log',backstep_seq);  % identify any back steps based on the search for this sequence
        
        backstep_ind_loc = (backstep_ind_st + 1)';
        
        back_steps = steps_temp(backstep_ind_loc,:);
        
        backstep_mat_rev = vertcat(backstep_mat_rev, back_steps);
        
end

backstep_stats_rev.num_backsteps = size(backstep_mat_rev,1);  % calculate probabilities

% backstep_stats_rev.prob_bkstep = backstep_stats_rev.num_backsteps/backstep_stats_rev.pos_steps_tot;

backstep_stats_rev.prob_bkstep = backstep_stats_rev.num_backsteps/backstep_stats_rev.neg_steps_tot;

end   % IMPORTANT: For calculating backstep probabilities for backsteps to the direction of unwinding, comment line 61 and uncomment line 64
      % For backstep probabilities of backsteps to the direction of
      % re-zipping, uncomment line 61 and comment line 63.
