function [pgauss, kern] = kd_bootstrap(tv, steps, step_err)
% short function that uses kernel density to estimate probability density
% of step sizes given the array of step sizes and the error on each step.
% can be adapted for things other than step sizes.  Since it doesn't plot
% the data, this function is primarily used with the
% Bootstrap_kernel_density code that creates a bootstrapped kernel density
% distribution from the resampled step size distributions

pgauss = [];
     
        for gg = 1:length(steps)
            pgauss(:,gg) = 1/(step_err(gg)*sqrt(2*pi)).*exp(-(tv'-steps(gg)).^2/(2*step_err(gg)^2));
        end
        kern = sum(pgauss,2);
        
end

