function [resamp,average,err] = Bootstrap_dwell(dwell_mat,limit)  % SPC 3/19/2018
% function to calculate average dwell times, standard deviation, and nmin
% by bootstrapping 

   % Makes dwell time cut based on lower limit for 
                                      % detectable dwell times
dwell_cut_mat = dwell_mat(dwell_mat(:,5) >= limit,:);   

dwell_cut = dwell_cut_mat(:,5);

% dwell_cut = dwell_mat(dwell_mat >= limit);

a = size(dwell_cut,1);

resamp.ave = zeros(1,10000);  % initialize variables for stats calculated
                              % from resamples
resamp.stdev = zeros(1,10000);  

resamp.nmin = zeros(1,10000);   % changed from zeros(10000,1) to (1,10000) on 12/13/2019, although it probably doesn't really matter

resamp.indices = zeros(a,10000);

for i = 1:1:10000     % do 10000 resamples, calculate stats for each one

    index = randi([1 a],a,1);
    
    dwells_new = dwell_cut(index);
    
    resamp.ave(i) = mean(dwells_new);
    
    resamp.stdev(i) = std(dwells_new);
    
    resamp.nmin(i) = ((mean(dwells_new))^2)/((std(dwells_new))^2);
    
    resamp.indices(:,i) = index;
    
end

average.ave = mean(resamp.ave);    % calculate average values for re-sample 
                                   % stats
average.stdev = mean(resamp.stdev);

average.nmin = mean(resamp.nmin);

average.nmin_alt = (average.ave)^2/(average.stdev)^2;

err.ave = std(resamp.ave);      % calculate standard errors using 
                                % standard deviation for resampled stats
err.stdev = std(resamp.stdev);

err.nmin = std(resamp.nmin);

[av_binsize, ~] = BinningStats_FreedmanDiaconis(resamp.ave,1);
[av_c, av_edge] = histcounts(resamp.ave,min(resamp.ave):av_binsize:max(resamp.ave));   % calculate binsizes for histograms with Freedman-Diaconis rule

[nmin_binsize, ~] = BinningStats_FreedmanDiaconis(resamp.nmin,1);
[nmin_c, nmin_edge] = histcounts(resamp.nmin,min(resamp.nmin):nmin_binsize:max(resamp.nmin));

[stdev_binsize, ~] = BinningStats_FreedmanDiaconis(resamp.stdev,1);
[stdev_c, stdev_edge] = histcounts(resamp.stdev,min(resamp.stdev):stdev_binsize:max(resamp.stdev));


datadirectory = 'C:\Users\seanc\Documents\step data reconfigured\ff and bb dwells\';

figure
histogram(resamp.ave, min(resamp.ave):av_binsize:max(resamp.ave))    % make histograms, save each figure
xlabel('average dwell time for each resample')
ylabel('counts')
title('Bootstrap average dwell distribution bb 1 uM ATP')
text(min(av_edge), max(av_c)*0.8, ['ave = ' num2str(average.ave,'%0.3f') ' s.e. = ' num2str(err.ave,'%0.3f') ' '])

fpath_save = datadirectory;
name_save = 'Bootstrap average dwell distribution bb 1 uM ATP';  % save figure
saveas(gca, fullfile(fpath_save, name_save));

figure
histogram(resamp.nmin, min(resamp.nmin):nmin_binsize:max(resamp.nmin))
xlabel('average Nmin for each resample')
ylabel('counts')
title('Bootstrap Nmin distribution bb 1 uM ATP')
text(min(nmin_edge), max(nmin_c)*0.8, ['ave = ' num2str(average.nmin,'%0.3f') ' s.e. = ' num2str(err.nmin,'%0.3f') ' '])

fpath_save = datadirectory;
name_save = 'Bootstrap nmin distribution bb 1 uM ATP';  % save figure
saveas(gca, fullfile(fpath_save, name_save));

figure
histogram(resamp.stdev, min(resamp.stdev):stdev_binsize:max(resamp.stdev))
xlabel('stdev for each resample')
ylabel('counts')
title('Bootstrap stdev distribution bb 1 uM ATP')
text(min(stdev_edge), max(stdev_c)*0.8, ['ave = ' num2str(average.stdev,'%0.3f') ' s.e. = ' num2str(err.stdev,'%0.3f') ' '])

fpath_save = datadirectory;
name_save = 'Bootstrap stdev distribution bb 1 uM ATP';  % save figure
saveas(gca, fullfile(fpath_save, name_save));
    
end

