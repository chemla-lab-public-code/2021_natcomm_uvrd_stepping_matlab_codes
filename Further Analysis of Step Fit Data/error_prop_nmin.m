function [nmin_error] = error_prop_nmin(ave,stdev,sem,nmin,length)
%short function thatcalculates error on nmin given mean, standard deviation,
%standard error, nmin, and length of vector containing dwells, for use
%with dwell_hist_calc

a = 2*(sem/ave)*(ave)^2;

b = (stdev)^2*sqrt((2)/(length - 1));

c = a/(ave^2);

d = b/(stdev^2);

e = sqrt(c^2 + d^2);

nmin_error = nmin*e;


end

