function [resamp_cut,counts] = Bootstrap_dwell_add_on(lower_lim, upper_lim, resamp, dwell_size) 
% Function designed to be used with Bootstrap_dwell to display exactly how 
% many times each individual dwell comes up in a certain population of resampled
% bootstrap arrays defined by lower and upper limits of nmin calculated for
% the resampled vector.  Useful in identifying outliers which skew the
% distribution of bootstrapped resampled nmin values to be multimodal.

resamp_cut = resamp.indices(:,resamp.nmin >= lower_lim & resamp.nmin <= upper_lim);

s = size(resamp_cut,2);        % makes cut and selects a certain proportion of resampled
                               % vectors that have nmin values which fall
                               % between a certain range specified by lower
                               % and upper limits  

counts.dwell_num = 1:1:dwell_size;

a = length(counts.dwell_num);

counts.dwell_freq_mat = zeros(a,s);

for i = 1:1:a
    
    for j = 1:1:s
        
        k = find(resamp_cut(:,j) == counts.dwell_num(i));  % figures out how often each dwell shows up in each of the
        
        counts.dwell_freq_mat(i,j) = length(k);            % bootstrap resamples
    end
end

counts.dwell_tot = sum(counts.dwell_freq_mat,2);   % sums across all bootstrap resample vectors to show how often a particular dwell
                                                   % shows up in the  population of resampled vector being looked at
    
end

